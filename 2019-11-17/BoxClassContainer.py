class Box:
    _queryQuestion = "What would you place in {0}? "
    _contentLabel = "Contents of {0}: {1}"
    inside = 'Nothing'

    def __init__(self, name, something=''):
        self.name = name
        if something != '':
            self.inside = something

    def put(self, something):
        self.inside = something

    def retrieve(self):
        return self.inside

    def query(self):
        self.inside = input(self._queryQuestion.format(
            self.name
        ))

    def pretty(self):
        print(self._contentLabel.format(
            self.name,
            self.inside
        ))