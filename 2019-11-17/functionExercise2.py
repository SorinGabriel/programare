def charOccurences(needle='', haystack='', minRequiredOccurences = 3):
    if len(needle) != 1:
        return 'First argument is invalid! Expected: character (1 length string)'

    occurences = haystack.count(needle)
    if(occurences < minRequiredOccurences):
        return f"'{needle}' character appears less than {minRequiredOccurences} time(s) in '{haystack}'!"
    return f"'{needle}' character occurences in '{haystack}': {occurences}"


print(charOccurences('a', 'stradaa'))
print(charOccurences('s', 'stradaa'))
print(charOccurences('c', 'stradaa'))
print(charOccurences('dc', 'stradaa'))
print(charOccurences('', 'stradaa'))
print(charOccurences('s', 'stradaa', 1))
print(charOccurences('f', 'stradaa', 1))

print(charOccurences())