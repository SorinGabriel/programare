def max_of_three(a, b, c):
    return max(a, b, c)


def max_of_three_custom(a=0, b=0, c=0):
    if (a >= b) and (a >= c):
        return a
    elif (b >= a) and (b >= c):
        return b
    else:
        return c


print(max_of_three(8, 1, 20))
print(max_of_three(80, 10, 20))
print(max_of_three(8, 100, 20))

print(max_of_three_custom(80, 10, 20))
print(max_of_three_custom(8, 100, 20))
print(max_of_three_custom(8, 1, 20))

print(max_of_three_custom())
print(max_of_three_custom(2, 2, 2))
print(max_of_three_custom(2, 4, 4))
print(max_of_three_custom(5, 5, 2))
print(max_of_three_custom(6, 8, 6))
print(max_of_three_custom(3, 4, 4))
print(max_of_three_custom(10, 9, 10))