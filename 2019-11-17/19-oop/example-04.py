class User:
    def __init__(self, name):
        self.name = name
        self.__password = ""

    @property  # getter
    def password(self):
        if len(self.__password) == 0:
            return ""
        secret = [ch for ch in self.__password]
        secret[1:-1] = ["*" for _ in range(len(secret) - 2)]
        return "".join(secret)

    @password.setter
    def password(self, new_pass):
        self.__password = f"{new_pass:=<6}"

    @password.deleter
    def password(self):
        del self.__password


if __name__ == "__main__":
    u = User("carl34")
    u.password = "s"
    print(u.password)
    u.password = "L0nger passwords ArE M0r3 Secure!"
    print(u.password)
