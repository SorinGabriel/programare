r"""
Modify Box class from example-02.py:
1. Add required argument to `__init__`, allowing for setting the 
   name of the box during initialization
2. Add optional argument to `__init__`, allowing for initialization
   of  `self.inside` with a string
3. Add a new method `query()`, which assigns a value  to `self.inside`
   using `input()`
4. Make common strings, like `"What would you place in Box"` and 
   `"Contents of Box"`, class variables
5. Add a new method `pretty()` which prints `Box`'s name and contents
"""


class Box:
    query_str = "What would you place in Box {}? "
    pretty_str = "Box {} contains: {}"

    def __init__(self, name, inside=""):
        self.name = name
        self.inside = inside

    def put(self, something):
        self.inside = something

    def retrieve(self):
        return self.inside

    def query(self):
        self.inside = input(self.query_str.format(self.name))

    def pretty(self):
        return self.pretty_str.format(self.name, self.inside)


if __name__ == "__main__":
    box_alpha = Box("alpha")
    box_alpha.put("a mystery")
    box_beta = Box("beta", inside="a secret")
    box_delta = Box("delta")
    box_delta.query()

    print(box_alpha.pretty())
    print(box_beta.pretty())
    print(box_delta.pretty())
