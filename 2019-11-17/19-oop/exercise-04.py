r"""
1. Create a new list `["Fido",` "Duke"]` and assign is to a variable `dogs`
>>> dogs = ["Fido", "Duke"]

2. Create a new binding of existing list to a new name: `my_dogs`
>>> my_dogs = dogs

3. Append a new dog to the list using any of the names
>>> dogs.append("K-9")

4. Print the list using both names. Are they the same?
>>> dogs == my_dogs
True
>>> dogs
['Fido', 'Duke', 'K-9']
>>> my_dogs
['Fido', 'Duke', 'K-9']

"""
