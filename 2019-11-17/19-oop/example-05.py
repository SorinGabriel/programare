fruit = ["apple"]
fruit2 = fruit

print(f"fruit id '{id(fruit)}', fruit2 id '{id(fruit2)}'")
print(f"fruit and fruit2 reference the same object: {fruit is fruit2}")


def add_fruit(list_of_fruit):
    list_of_fruit.append("orange")


add_fruit(fruit2)
print(f"fruit id '{id(fruit)}', fruit2 id '{id(fruit2)}'")
print(f"fruit and fruit2 reference the same object: {fruit is fruit2}")
print(f"fruit: {fruit}")
