class Record:
    str_format = "{0} ({1}) by {2}"

    def __init__(self, title, year, artist):
        print(f"Called __init__ for {title}")
        self.title = title
        self.year = year
        self.artist = artist

    def pretty(self):
        return self.str_format.format(self.title, self.year, self.artist)


records = [
    Record("Led Zeppelin IV", 1971, "Led Zeppelin"),
    Record("Brothers in Arms", 1985, "Dire Straits"),
]

for r in records:
    print(r.pretty())
