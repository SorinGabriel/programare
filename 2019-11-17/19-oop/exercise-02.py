r"""
1. Define a new class `User` with two fields: `name` and `_password`
2. Make sure `password` is a private field using a single `_`
3. `name` has to be supplied during initialization
4. Try printing `u._password` outside of class scope. Does it work?
5. Add `set_password()` method, that allows user to change password. 
   The password has to be at least 6 characters long. If it is 
   shorter, it is filled with `=` up to 6 characters.
6. Add `get_password()` method, which allows user to display 
   password with only first and last letter visible, the rest 
   substituted with `*` symbols.
7. Change `_password` definition, so that its name is mangled with 
   class name: `__password`
8. Try printing `u.__password` outside of class scope. Does it work?
"""


class User:
    def __init__(self, name):
        self.name = name
        self.__password = ""
        # This is never used inside class. It's a showcase for 4.
        self._showcase_password = "showcase"

    def set_password(self, new_pass):
        self.__password = f"{new_pass:=<6}"

    def get_password(self):
        if len(self.__password) == 0:
            return ""
        secret = [ch for ch in self.__password]
        secret[1:-1] = ["*" for _ in range(len(secret) - 2)]
        return "".join(secret)


if __name__ == "__main__":
    u = User("carl34")
    u.set_password("s")
    print(u.get_password())
    u.set_password("L0nger passwords ArE M0r3 Secure!")
    print(u.get_password())

    print(f"Showcase password: {u._showcase_password}")
    try:
        print(u.__password)
    except AttributeError:
        print("Trying to access u.__password: AttributeError.")
