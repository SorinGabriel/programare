class Flower:
    pass


dandelion = Flower()
rose = Flower()

print(f"Rose and dandelion the same object: {rose == dandelion}")
print(f"Rose's type: {type(rose)}, dandelion's type: {type(dandelion)}")
print(
    "Rose and dandelion are instances of the same class - Flower: ",
    f"{isinstance(rose, Flower) == isinstance(dandelion, Flower)}",
)
