class Box:
    def put(self, something):
        self.inside = something

    def retrieve(self):
        return self.inside


box_a = Box()
box_b = Box()

box_a.put(input("What would you place in Box A? "))
box_b.put(input("What would you place in Box B? "))

print(f"Contents of Box A: {box_a.retrieve()}")
print(f"Contents of Box B: {box_b.retrieve()}")
