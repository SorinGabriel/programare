r"""
1. Create a new dictionary. Print its id.
>>> d = {}
>>> prev_id = id(d)

2. Update the dictionary, check its id. Is it the same?
>>> d.update({"new": "pair"})
>>> id(d) == prev_id
True

3. Create a new string. Print its id.
>>> s = "Base"
>>> prev_id = id(s) 

4. Add another string to it using `+` operator. Check new string's id.
>>> s += " string"
>>> prev_id != id(s)
True

5. extracurricular exercise - for the curious
    1. create variable `a = 5`
    2. create variable `b = 2` `+` `3`
    3. create variable `c = 300`
    4. create variable `d = 3 * 100`
    5. Compare IDs of `a`, `b`, `c` and `d`. What are they? Why is that? Ask a tutor to explain.
>>> a = 5
>>> b = 2 + 3
>>> c = 300
>>> d = 3 * 100
>>> id(a) == id(b) and a is b
True
>>> id(c) != id(d) and c is not d
True

Explanation from current CPython implementation:
(source: https://github.com/python/cpython/blob/master/Doc/c-api/long.rst)

   The current implementation keeps an array of integer objects for all integers
   between ``-5`` and ``256``, when you create an int in that range you actually
   just get back a reference to the existing object.
"""
