class CustomClass:
    class_var = ""  # class variable

    def __init__(self):  # special method
        self.field = ""  # field

    def get_data(self):  # method
        return self.class_var, self.field
