from BoxClassContainer import Box
# box_a = Box("Demo Box 1")
# box_b = Box("Demo Box 2")

# box_a.put(input(f"What would you place in {box_a.name}? "))
# box_b.put(input(f"What would you place in {box_b.name}? "))

# print(f"Contents of {box_a.name}: {box_a.retrieve()}")
# print(f"Contents of {box_b.name}: {box_b.retrieve()}")

# Instantiate a Box object with name and content
box_c = Box("Demo Box 3", "Scissors")
box_c.pretty()

# Instantiate a Box object with name
box_d = Box("Demo Box 4")
# Set the content from user input
box_d.query()
box_d.pretty()

# Instantiate a Box object with name and content
box_e = Box("Demo Box 5")
box_e.pretty()
