import unittest


class DemoTest(unittest.TestCase):
    def test_demo(self):
        self.assertIn('a', 'abc')

    def test_demo2(self):
        self.assertIsInstance('a', str)

    def test_demo3(self):
        self.assertFalse(False)

    def test_demo4(self):
        self.assertDictEqual({'abc':1}, {'abc':1})

