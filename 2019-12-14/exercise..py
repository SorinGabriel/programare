import math

class Fig2d:
    def __init__(self, lat=[]):
        self.lat = lat

class Lat4(Fig2d):
    lenLats = 4
    def perim(self):
        return sum(self.lat[0:self.lenLats])

class Drept(Lat4):
    lenLats = 2
    def perim(self):
        return 2 * super().perim()

class Patrat(Lat4):
    lenLats = 1
    def perim(selfs):
        return 4 * super().perim()

# Four segments geomtric figure, must take a list of 4 params each representing a segment length
test = Lat4([1, 2, 2, 5, 7899])
print(f"Perimeter of a figure with 4 segments with following defined lats {test.lat} is " + str(test.perim()))
# Rectangle - takes a list of 2 params because 2 pairs of segments are equal
test2 = Drept([23, 10, 2222])
print(f"Perimeter of a rectangle with following defined lats {test2.lat} is " + str(test2.perim()))

test3 = Patrat([1, 1000])
print(f"Perimeter of a square with following defined lats {test3.lat} is " + str(test3.perim()))
print(2 * [2, 5, 10])

class Cerc(Fig2d):

    # perimetrul cercului == lungimea cercului
    def perim(self):
        return 2 * math.pi * self.lat[0]

# Circle - radius
test4 = Cerc([5, 1213])
print(f"Perimeter of a circle with following defined lats {test4.lat} is " + str(test4.perim()))
