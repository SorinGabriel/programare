class Animal:
    def __init__(self, name, age):
        self._name = name
        self._age = age

    @property
    def name(self):
        return f"My name is {self._name}"

    @property
    def age(self):
        return f"My age is {self._age} years"

    def make_a_sound(self):
        return ''


class Dog(Animal):
    def __init__(self, name, age):
        super().__init__(name, age)

    @property
    def name(self):
        return f"My name is {self._name}, the dog."

    def make_a_sound(self):
        return "woof!"

class Cat(Animal):

    def make_a_sound(self):
        return "meow!"

    @property
    def name(self):
        return super().name +" the cat."




if __name__ == "__main__":
    print(f"Dog is a subclass of Animal: {issubclass(Dog, Animal)}")
    print(f"Animal is a subclass of object: {issubclass(Animal, object)}")
    d = Dog("Rex", 1536)
    print(d.name)
    print(d.age)
    print(f"d is an instance of Dog: {isinstance(d, Dog)}")
    print(f"d is an instance of Animal: {isinstance(d, Animal)}")
    print(f"d is an instance of object: {isinstance(d, object)}")
    cat = Cat('test', 1900)
    print(cat.name)
    print(cat.age)