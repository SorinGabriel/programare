from sqlalchemy.orm import sessionmaker
from models import Base, Task, eng

"""
● For show tasks:
    ○ print all open tasks and their ids in order of
    ids
● For mark as done
    ○ ask user which id to mark as done
    ○ update the done field in the table for given id
● For add new task
    ○ ask for task name/description
    ○ insert a new record to the tasks db
"""

Session = sessionmaker(bind=eng)
s = Session()

def add_task():
    """
    ask for task name/description and insert a new not task record marked as not done
    :return:
    """
    try:
        s.add(
            Task(
                task=input("Enter task name/ description: "),
                done=0
            )
        )
        s.commit()
        print("Task successfully inserted.")
    except Exception as e:
        print(e)


def show_tasks():
    """
    Print the list of available tasks and their total number
    :return:
    """
    rows = s.query(Task).all()
    print()
    for row in rows:
        print(row)
    print("---")
    total = s.query(Task).count()
    print(f"Total: {total}")
    print()


def mark_task_as_done():
    """
    Ask the user for a task ID and set it as done if it exists
    :return:
    """
    print()
    input_id = input("Enter task ID: ")
    if input_id.isnumeric():
        task = s.query(Task).filter_by(id=int(input_id)).first()
        if task:
            task.done = 1
            s.commit()
            print("Task successfully updated.")
            print()
        else:
            print("Warning! Task not found.")
            print()
    else:
        print("Warning! Provided ID is not numeric")
        print()