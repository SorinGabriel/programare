"""
● Create a todo_app database
● Create a tasks table with the following schema
    ○ id int not null auto_increment
    ○ task text not null
    ○ done boolean
    ○ primary key - id
"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text, Boolean
from sqlalchemy.orm import validates
from db_connect import eng

Base = declarative_base()
Base.metadata.create_all(eng)


class Task(Base):
    __tablename__ = "tasks"
    id = Column(Integer, primary_key=True, autoincrement=True)
    task = Column(Text)
    done = Column(Boolean)

    @validates('task')
    def validate_task(self, key, task):
        if task.strip() == '':
            raise AssertionError("Task can not be empty")
        return task

    def __str__(self):
        return f"<Task #{self.id} {self.task}; Done: {self.done}>"


