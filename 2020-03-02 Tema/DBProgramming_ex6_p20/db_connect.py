from sqlalchemy import create_engine

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(
        user="root", password="123456", host="127.0.0.1", db="default"
    )
)
