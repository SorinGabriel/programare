from task_functions import *

"""
● In a loop:
    ○ ask user what to do using input()
    ○ show task list
    ○ mark task as done
    ○ add new task
    ○ exit application
"""

while True:
    command = input(
        """Please select an action. Actions - (s) show task list, (d) mark task as done, (a) add new task, (x) exit application: """)

    if command == 's':
        show_tasks()
    elif command == 'd':
        mark_task_as_done()
    elif command == 'a':
        add_task()
    elif command == 'x':
        break
    else:
        print("Unknown command, please try again")