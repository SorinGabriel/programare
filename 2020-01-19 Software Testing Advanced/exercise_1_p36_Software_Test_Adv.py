from unittest.mock import Mock


def test_db_connection_mock():
    db=Mock()
    db.count.return_value = 1
    assert db.count() == 1

    db.list_tables.return_value = (
        "users"
    )
    assert db.list_tables() == (
        "users"
    )

    db.get.return_value = {
        'username' : '',
        'first_name' : '',
        'last_name': '',
        'email': ''
    }
    assert db.get() == {
        'username' : '',
        'first_name' : '',
        'last_name': '',
        'email': ''
    }
