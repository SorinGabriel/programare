import random
import string

def lowercase(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs).lower()

    return wrapper

def uppercase(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs).upper()

    return wrapper

def shorten(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)[:40]

    return wrapper

# Execution order: shorten and then lowercase
@uppercase
@lowercase
@shorten
def random_string(length=60):
    return ''.join(random.choices(string.ascii_uppercase, k=length))


print(random_string(90))
print(len(random_string(17)))