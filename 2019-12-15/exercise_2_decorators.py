import time

class ExecutionTime():
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        start_time = time.time()
        result = self.func(*args, **kwargs)
        end_time = time.time()
        print(f"Execution time is {end_time - start_time}s")
        return result

@ExecutionTime
def loopPower(x, y):
    for i in range(1000001):
        x**y

@ExecutionTime
def fibonacci(n=0):
    values = [0, 1]
    if n < 0:
        return
    elif n == 0:
        return values[0]
    for length in range(1, n):
        temp = values[1]
        values[1] = values[1] + values[0]
        values[0] = temp
    return values[1]



#loopPower(2, 3)
print(fibonacci(1))
print(fibonacci(100000))