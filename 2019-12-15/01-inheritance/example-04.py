import json


class JSONOutput:
    def __init__(self, *args, **kwargs):
        pass

    def to_json(self):
        return json.dumps(self)


class SimpleRow(dict, JSONOutput):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _headers(self):
        header_width = max(len(str(k)) for k in self)
        headers = " | ".join(str(k).center(header_width) for k in self)
        return f"| {headers} |"

    def _values(self):
        header_width = max(len(str(k)) for k in self)
        values = " | ".join(str(v).center(header_width) for v in self.values())
        return f"| {values} |"

    def table(self):
        return f"{self._headers()}\n{self._values()}"

class SimpleTable(list, JSONOutput):
    def table(self):
        return '\n'.join(sampleRow.table() if i == 0 else sampleRow._values() for i, sampleRow in enumerate(self))


if __name__ == "__main__":
    row = SimpleRow(no=1, name="Mark", surname="O'Connor", nationality="Irish")
    #print(row.table())
    print(row.to_json())
    table = SimpleTable()
    table.append(row)

    row2 = SimpleRow(no=2, name="Marke", surname="O'Connor2", nationality="Irishh")
    row3 = SimpleRow(no=3, name="Markee", surname="O'Connor3", nationality="Irishhh")

    table.append(row2)
    table.append(row3)
    print(table)
    print(table.table())
    print(table.to_json())