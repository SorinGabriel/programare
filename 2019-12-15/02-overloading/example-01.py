class EshopCart:
    def __init__(self, buyer):
        self.buyer = buyer
        self.products = []
        self.total = 0.0

    def add_product(self, name, price):
        self.products.append(name)
        self.total += price

    def __len__(self):
        return len(self.products)

    def __add__(self, cart):
        self.products.extend(cart.products)
        self.total += cart.total
        return self

if __name__ == "__main__":
    cart = EshopCart("Ann")
    cart.add_product("jeans", 30.0)
    print(f"Cart's length: {len(cart)}")

    cart2 = EshopCart("Test")
    cart2.add_product("p1", 10.0)
    cart2.add_product("p2", 40.0)
    cart+ cart2
    print("After adding cart2 to cart:")
    print(len(cart))
    print(cart.total)
    print(cart.products)

    cart3 = EshopCart("Test")
    cart3.add_product("p1", 230.0)
    cart3.add_product("p2", 15.0)

    cart4 = EshopCart("Test")
    cart4.add_product("p1", 99.9)
    cart4.add_product("p2", 49.9)
    cart + cart3 + cart4
    print("After adding cart3 and cart4 to cart:")
    print(len(cart))
    print(cart.total)
    print(cart.products)

    print(True >= False)
    print(False >= False)
    print(False >= True)