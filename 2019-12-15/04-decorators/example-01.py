import functools


def example_decorator(func):
    """ 10 """
    # @functools.wraps(func)
    def wrapper(*args, **kwargs):
        """ 5 """
        print("Wrapper: Before function execution")
        result = func(*args, **kwargs)
        print("Wrapper: After function execution")
        return result

    return wrapper


@example_decorator
def greetings(name):
    """ 1 """
    print(f"Hello, {name}!")


if __name__ == "__main__":
    greetings("Jane")
    print(greetings.__doc__)
