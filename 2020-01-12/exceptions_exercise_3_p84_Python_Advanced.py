import logging


# 1 Define your own exceptions


class PasswordTooShort(Exception):
    """ Child of Exception class thrown when provided password has too few characters """
    pass


class PasswordTooLong(Exception):
    """ Child of Exception class thrown when provided password has too many characters """
    pass


class InvalidPassword(Exception):
    """ Child of Exception class thrown when provided password does not match the correct password """
    pass


class SpecialCharacters(Exception):
    """ Child of Exception class thrown when provided password contains special characters"""
    pass


# 2 Write a function validate_password()

def validate_password(password=''):
    secret_password = "test@#"
    password_length = len(password)

    if password_length < 3:
        raise PasswordTooShort("Password must contain at least 3 characters!")
    elif password_length > 30:
        raise PasswordTooLong("Password must contain no more than 30 characters!")
    else:

        for character in password:
            if not character.isalnum():
                if character in ["@", "/", "'", "#", "$", "!"]:
                    logging.warning("Password has special characters in it!")
                    break
                else:
                    raise SpecialCharacters("Password contains forbidden special characters!")

        if password.lower() == secret_password.lower():
            for character in password:
                if character.isupper():
                    return {
                        'success': False,
                        'message': 'Correct password but caps lock is activated as it has upper case character in it!'
                    }

            return {
                'success': True,
                'message': 'Correct password!'
            }
        else:
            raise InvalidPassword("Password does not match the secret password!")


if __name__ == "__main__":
    l = logging.getLogger()
    l.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(
        logging.Formatter("%(levelname)-8s (%(asctime)s): %(message)s")
    )
    l.addHandler(console_handler)

    while True:
        userInput = input("Please enter the password: ")
        if userInput == 'q':
            logging.info("Quitting loop!")
            break
        else:
            try:
                validationResults = validate_password(userInput)
                if validationResults['success']:
                    logging.info(validationResults['message'])
                    logging.info("Logging in!")
                    break
                else:
                    logging.error(validationResults['message'])
            except Exception as e:
                logging.error(e)
