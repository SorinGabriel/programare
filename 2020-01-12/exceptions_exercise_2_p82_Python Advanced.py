import random

def random_exception():
    raise random.choice(Exception.__subclasses__())


if __name__ == "__main__":
    exceptions = {exception.__name__: 0 for exception in Exception.__subclasses__()}

    for _ in range(100):
        try:
            random_exception()
        except Exception as inst:
            exceptions[inst.__class__.__name__] += 1

    print(exceptions)
