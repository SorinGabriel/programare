
class ChildMemoryException(MemoryError):
    pass

if __name__ == "__main__":
    # Exercise 1 - raise an exception
    # raise MemoryError("Stack overflow")

    try:
        print("Test")
        raise ChildMemoryException("test")
    except MemoryError:
        print("Works")