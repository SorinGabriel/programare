class Decorator2(object):
    def __init__(self, arg):
        self.arg = arg

    def __call__(self, cls):
        class Wrapped(cls):
            classattr = self.arg

            def new_method(self, value):
                return value * 8

        return Wrapped


class Decorator(object):
    def __init__(self, arg):
        self.arg = arg

    def __call__(self, cls):
        class Wrapped(cls):
            classattr = self.arg

            def new_method(self, value):
                return value * 2

        return Wrapped


@Decorator2('asd')
@Decorator("decorated class")
class TestClass(object):
    def new_method(self, value):
        return value * 3


a = TestClass()

print(a.new_method(2))


class A:
    def __init__(self, a='a'):
        print(a)

    def __call__(self, b='b'):
        print(b)

    def x(self):
        print('x')


a = A()
a()
a.x()

print(dir(a))

print({k: k for k in ('a', 'b', 'c')})
