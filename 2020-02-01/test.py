from functools import reduce


class A:

    def __init__(self):
        pass

    def x(self):
        print('x')


class B(A):

    def __init__(self):
        pass

    def x(self):
        print('y')


c = B()
c.x()

print(reduce(sum, [1, 2, 3, 4]))
print()
L = lambda a, b: a + b
print(L)
L = lambda a: a * b

print(L)

L = [1, 2]

print(L)
x = L.__iter__()
print(x.__next__())

print(x.__next__())
# print(x.__next__())


str = "+1-202-555-0193"

print(str.rfind('[0-9]+'))
print(str.rfind('[[0-9]+]+'))
print(str.rfind('[0-9+-]*'))
print(str.rfind('[[0-9]+]+'))

import multiprocessing



