"""
1. Given car list from example-04.py, find out the following using reduce-filter or mapfilter-
reduce:
"""

from functools import reduce


class Car:
    def __init__(self, make, model, price):
        self.make = make
        self.model = model
        self.price = price


cars = [
    Car("Ford", "Anglia", 300.0),
    Car("Ford", "Cortina", 700.0),
    Car("Ford", "Cortina 2", 700.0),
    Car("Ford", "Cortina 3", 700.0),
    Car("Alfa Romeo", "Stradale 33", 190.0),
    Car("Alfa Romeo", "Giulia", 500.0),
    Car("Citroën", "2CV", 75.0),
    Car("Citroën", "Dyane", 105.0),
]

# Let's find the price of the cheapest Citroën on the list
c = reduce(
    min, map(lambda car: car.price, filter(lambda car: car.make == "Citroën", cars))
)
print(f"The cheapest Citroën costs: {c}")
print()

"""
2. What is the total cost of all Alfa Romeos on the list?
"""
print("2. What is the total cost of all Alfa Romeos on the list?")
d = reduce(
    lambda x, y: x + y, map(lambda car: car.price, filter(lambda car: car.make == 'Alfa Romeo', cars))
)
print(f"{d}")
print()
"""
3. What is the count of all Fords on the list? Hint: use initial = 0
"""
e = reduce(
    lambda x, y: x + y, map(lambda car: 1, filter(lambda car: car.make == 'Ford', cars))
 )
print("3. What is the count of all Fords on the list? Hint: use initial = 0")
print(f"{e}")
