"""
Using example-03.py as reference, implement iterator MyRange which behaves just like
built-in range(start, stop, step)
"""
import unittest

# Implementation of a custom iterator
class MyRange:
    def __init__(self, start, stop=0, step=1):
        if stop == 0:
            self.i = 0
            self.stop = start
        else:
            self.i = start
            self.stop = stop

        self.range = step

    def __iter__(self):  # called when iterator is created
        return self

    def __next__(self):  # called on next
        value = self.i
        if self.i < self.stop:
            self.i += self.range
            return value
        else:
            raise StopIteration


class MyRange2:
    def __init__(self, start, stop, step):
        self.position = start
        self.stop = stop
        self.step = step

    def __iter__(self):  # called when iterator is created
        self.position -= self.step
        return self

    def __next__(self):  # called on next
        self.position += self.step
        if self.position < self.stop:
            return self.position
        else:
            raise StopIteration

class TestMyRange(unittest.TestCase):
    def test_my_range(self):
        for start in range(0, 10):
            for stop in range(10, 51, 10):
                for step in range(1, 15):
                    self.assertEqual(list(MyRange(start, stop, step)), list(range(start, stop, step)))

    def test_my_range2(self):
        for start in range(0, 10):
            for stop in range(10, 51, 10):
                for step in range(1, 15):
                    self.assertEqual(list(MyRange2(start, stop, step)), list(range(start, stop, step)))
