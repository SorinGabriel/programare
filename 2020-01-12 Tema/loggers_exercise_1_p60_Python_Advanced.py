"""
Given file exercise-01.py
    1. Convert all calls to `print()` to proper logging
    2. configure logging to log simultaneously to two files and console, using the following formats and levels:
        1. File A: `timestamp - level - message` level DEBUG
        2. File B: `filename - funcname - line number` level INFO
        3. Console: `asctime [level]: message` level WARN
"""

import time
import random
import functools
import logging

def decorate(func):
    def wrapper(*args, **kwargs):
        logging.info("calling a function")
        return func(*args, *kwargs)

    return wrapper


@decorate
def complex_calculation(a, b):
    logging.debug("performing complex calculation")
    result = a + b
    logging.debug(f"result={result}")
    return result


if __name__ == "__main__":
    l = logging.getLogger()

    file_handler = logging.FileHandler("log_file_A.log", mode="w+")
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(
        logging.Formatter("%(asctime)-8s - %(levelname)s - %(message)s")
    )
    l.addHandler(file_handler)

    file_handler2 = logging.FileHandler("log_file_B.log", mode="w+")
    file_handler2.setLevel(logging.INFO)
    file_handler2.setFormatter(
        logging.Formatter("%(filename)-8s - (%(funcName)s) - %(lineno)s")
    )
    l.addHandler(file_handler2)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.WARN)
    console_handler.setFormatter(
        logging.Formatter("%(asctime)-8s [%(levelname)s]: %(message)s")
    )
    l.addHandler(console_handler)

    for _ in range(10):
        result = complex_calculation(random.randint(0, 15), random.randint(0, 15))
        if result <= 10:
            logging.warning("a warning")
        elif result <= 20:
            logging.error("an error")
        elif result <= 30:
            logging.critical("a critical error")
