var myName = 'ExampleName';
console.log('Hello ' + myName +  '!');

var text = "";
var i;
for (i = 0; i < 5; i++) {
	text += " The number is " + i + " ";
}


while (i <= 10) {
	if (i % 2 === 0) {
		text += " The number is " + i;
	}
	i++;
}

console.log(text);

function fibonacci(num){
	var current = 0, next = 1, temp;
	num = num;
	while (num > 0){
		temp = next;
		next = next + current;
		current = temp;
		num--;
	}

	return current;
}

console.log("1th number in the Fibonacci sequence is: " +fibonacci(0));
console.log("2th number in the Fibonacci sequence is: " +fibonacci(1));
console.log("3th number in the Fibonacci sequence is: " +fibonacci(2));
console.log("4th number in the Fibonacci sequence is: " +fibonacci(3));
console.log("5th number in the Fibonacci sequence is: " +fibonacci(4));
console.log("6th number in the Fibonacci sequence is: " +fibonacci(5));
console.log("7th number in the Fibonacci sequence is: " +fibonacci(6));
console.log("8th number in the Fibonacci sequence is: " +fibonacci(7));
console.log("9th number in the Fibonacci sequence is: " +fibonacci(8));
console.log("10th number in the Fibonacci sequence is: " +fibonacci(9));
console.log("11th number in the Fibonacci sequence is: " +fibonacci(10));



