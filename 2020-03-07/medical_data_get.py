import pymongo
import json

client = pymongo.MongoClient("127.0.0.1", 27017)

db = client.medical_db
collection = db.medicaldata
"""
● Find all rows with procedure_code equal 0F1F4ZC
"""
print('Rows with procedure_code equal to "0F1F4ZC":')
for x in collection.find({"procedure_code": '0F1F4ZC'}):
    print(x)
print()
"""
● Find patient with patient_id equal 74, print his full name
"""
for x in collection.find({"patient_id": 74}):
    print('Full name of patient_id equal to 74: '+ str(x['first_name'] + ' ' + str(x['last_name'])))
print()
"""
● Find a procedure performed on 2019-05-24T01:52:37.000Z and update its procedure code to 0F1F4ZC
"""
print('Procedure performed on 2019-05-24T01:52:37.000Z: ')
for x in collection.find({"visit_date.date": "2019-05-24T01:52:37.000Z"}):
    print(x)
print()

myquery = {"visit_date.date": "2019-05-24T01:52:37.000Z"}
newvalues = { "$set": { "procedure_code": "0F1F4ZC" } }

x = collection.update_many(myquery, newvalues)

