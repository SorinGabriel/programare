import pymongo
import json

client = pymongo.MongoClient("127.0.0.1", 27017)

db = client.movies
collection = db.imdb
"""
1. Cate premii a luat "Bound"?
"""
for x in collection.find({"title": 'Bound'}):
    print(f'Bound ' + str(int(x['year'])) + ' had ' + str(int(x['awards']['wins'])) + ' wins')
print()
"""
1". Cate nominalizari a avut "The Big Lebowski"
"""
for x in collection.find({"title": "The Big Lebowski"}):
    print(str(x['title']) + ' ' + str(int(x['year'])) + ' had ' + str(int(x['awards']['nominations'])) + ' nominations')
print()
"""
2. Distributia la filmul "Blade II"
"""
for x in collection.find({"title": "Blade II"}):
    print(str(x['title']) + ' ' + str(int(x['year'])) + ' had the following actors: ' + str(x['actors']))
print()
"""
3. In ce tari a rulat Rocky?
"""
for x in collection.find({"title": {'$regex': 'Rocky'}}):
    print(str(x['title']) + ' ' + str(int(x['year'])) + ' ran in the following countries: ' + str(x['countries']))
print()
"""
4. In ce filme a jucat Arnold? 
"""
for x in collection.find({"actors": 'Arnold Schwarzenegger'}):
    print('Arnold S played in: ' + str(x['title']) + ' ' + str(int(x['year'])))
print()

"""
5. Ce filme au castigat sau au fost nominalizate la Bafta? 
"""
print('Movies that had wins or got nominated by Bafta:')
for x in collection.find({'awards.text': {'$regex': 'BAFTA'}}):
    print(str(x['title']) + ' ' + str(int(x['year'])))
print()
