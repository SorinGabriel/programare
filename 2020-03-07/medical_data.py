import pymongo
import json
"""
● Use data in medical-data.json to create a new collection: medicaldata
"""
client = pymongo.MongoClient("127.0.0.1", 27017)

db = client.medical_db
collection = db.medicaldata
collection.remove()
with open("medical-data.json") as f:
    data = json.load(f)
    for obj in data:
        obj['visit_date']['date'] = obj['visit_date']['$date']
        del obj['visit_date']['$date']
        collection.insert_one(obj)

cursor = collection.find()
print(cursor)
for document in cursor:
  print(document)

print(f"Records count: {collection.count()}")