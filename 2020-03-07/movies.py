import pymongo
import json

client = pymongo.MongoClient("127.0.0.1", 27017)

db = client.movies
collection = db.imdb
collection.remove()
with open("movieDetails.json", encoding='utf-8') as f:
    for line in f:
        j_content = json.loads(line)
        del j_content['_id']
        response = collection.insert_one(j_content)


cursor = collection.find()
print(cursor)
for document in cursor:
  print(document)

print(f"Records count: {collection.count()}")