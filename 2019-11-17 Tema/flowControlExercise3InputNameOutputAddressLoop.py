address_book = {
    "Helen": "838 Tanglewood Road\nSalisbury, MD 21801",
    "Theresa": "1224 Abia Martin Drive\nWebster, WI 54893",
    "John": "2742 Brownton Road\nPompano Beach, FL 33069",
    "William": "531 Hillhaven Drive\nHollywood, CA 90028",
}

if __name__ == "__main__":

    name = ''
    while True:
        name = input("Please enter a name or enter 'quit' to exit the loop: ")
        if name == "":
            print("Name can not be empty!")
            continue
        elif name == "quit":
            print("Exiting loop")
            break
        elif name in address_book:
            print(f"{name} lives at:\n{address_book[name]}")
        else:
            print(f"I don't have {name} in my address book")
