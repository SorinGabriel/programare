#1
print("Exercise 1 -----")
zippedData = zip(
        {
            "key1": "value1",
            "key2": "value2",
            "key3": "value3",
        },
        (
            "item1",
            "item2",
            "item3"
        ),
        [
            "lItem1",
            "lItem2",
            "lItem3"
        ]
    )
print(zippedData)
print(list(zippedData))
print(dict(zippedData))

#2
print("Exercise 2 -----")
zippedData = zip(
    [1, 2, 3, 4],
    ('a', 'b', 'c')
)
print(zippedData)
print(list(zippedData))
print(dict(zippedData))

#3
print("Exercise 3 -----")
zippedData = zip(
    [1, 2, 3, 4]
)
print(zippedData)
print(list(zippedData))
print(dict(zippedData))

#4
print("Exercise 4 -----")
zippedData = zip()
print(zippedData)
print(list(zippedData))
print(dict(zippedData))