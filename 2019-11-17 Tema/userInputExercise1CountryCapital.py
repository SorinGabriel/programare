countryCapitalDictionary = {}
while len(countryCapitalDictionary) < 3:
    country = input("State a country: ")
    capital = input(f"State {country}'s capital: ")
    countryCapitalDictionary[country] = capital

print(f"Country - capital dictionary is: {countryCapitalDictionary}")