import sys

address_book = {
    "Helen": "838 Tanglewood Road\nSalisbury, MD 21801",
    "Theresa": "1224 Abia Martin Drive\nWebster, WI 54893",
    "John": "2742 Brownton Road\nPompano Beach, FL 33069",
    "William": "531 Hillhaven Drive\nHollywood, CA 90028",
}

if __name__ == "__main__":
    names = sys.argv[1:]
    if len(names) < 1:
        print("You must provide at least one argument!\n")
        sys.exit(1)
    for name in names:
        if name in address_book:
            print(f"{name} lives at:\n{address_book[name]}\n")
        else:
            print(f"I don't have {name} in my address book\n")