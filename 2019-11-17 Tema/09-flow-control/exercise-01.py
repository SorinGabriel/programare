"""
Create the following script:
    1. It takes exactly one argument (check using sys.argv, len, if-else)
    2. If the number of arguments is different from one it prints a warning and exits using sys.exit(1)
    3. The argument is a string containing a person’s name
    4. Given the dictionary `address_book`, checks whether the person’s address is known
    5. If it is known, print the address
    6. If it is unknown, print a message informing about this
"""
import sys


address_book = {
    "Helen": "838 Tanglewood Road\nSalisbury, MD 21801",
    "Theresa": "1224 Abia Martin Drive\nWebster, WI 54893",
    "John": "2742 Brownton Road\nPompano Beach, FL 33069",
    "William": "531 Hillhaven Drive\nHollywood, CA 90028",
}

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"You have provided {len(sys.argv)-1} arguments when 1 was expected!")
        sys.exit(1)

    name = sys.argv[1]
    if name in address_book:
        print(f"{nme} lives at:\n{address_book[name]}")
    else:
        print(f"I don't have {name} in my address book")
