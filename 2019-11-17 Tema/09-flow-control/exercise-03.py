r"""
1. Modify an application from 09-flow-control/flowControlExercise3InputNameOutputAddressLoop.py
2. query user to input the name (use input function)
3. exit if the input is “quit”, try to display address otherwise
4. continue to next loop if the name is empty
5. use while True loop, continue statement, and break statement
"""
import sys


address_book = {
    "Helen": "838 Tanglewood Road\nSalisbury, MD 21801",
    "Theresa": "1224 Abia Martin Drive\nWebster, WI 54893",
    "John": "2742 Brownton Road\nPompano Beach, FL 33069",
    "William": "531 Hillhaven Drive\nHollywood, CA 90028",
}

if __name__ == "__main__":
    while True:
        name = input("Please provide a name: ")
        if name == "quit":
            print("Quitting")
            break
        elif name == "":
            print("The name you provided is empty!")
            continue

        if name in address_book:
            print(f"{name} lives at:\n{address_book[name]}")
        else:
            print(f"I don't have {name} in my address book")
