r"""
print a sum of all even numbers between 2019 and 2500
"""
total = 0
for i in range(2020, 2500, 2):
    total += i
print(f"The sum of all even numbers between 2019 and 2500 is {total}")
