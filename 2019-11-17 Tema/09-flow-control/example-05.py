while True:
    print("Say 'yes' if I should break the loop")
    decision = input("> ")
    if decision == "yes":
        print("I'm breaking the loop")
        break

for i in range(5):
    if i == 3:
        print(f"Skipping iteration #{i}")
        continue
    print(f"Iteration #{i}")

print("Finish")
