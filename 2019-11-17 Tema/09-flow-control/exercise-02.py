r"""
1. Modify an application from 09-flow-control/flowControlExercise3InputNameOutputAddressLoop.py
2. try to display address for every name supplied in sys.argv
3. use for loop
"""
import sys


address_book = {
    "Helen": "838 Tanglewood Road\nSalisbury, MD 21801",
    "Theresa": "1224 Abia Martin Drive\nWebster, WI 54893",
    "John": "2742 Brownton Road\nPompano Beach, FL 33069",
    "William": "531 Hillhaven Drive\nHollywood, CA 90028",
}

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print(f"You have provided no arguments when at least 1 was expected!")
        sys.exit(1)

    for name in sys.argv[1:]:
        if name in address_book:
            print(f"{name} lives at:\n{address_book[name]}")
        else:
            print(f"I don't have {name} in my address book")
