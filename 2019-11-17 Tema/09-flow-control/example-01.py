day = int(input("Select day of the week (1-7): "))
weekday = {
    1: "Monday",
    2: "Tuesday",
    3: "Wednesday",
    4: "Thursday",
    5: "Friday",
    6: "Saturday",
    7: "Sunday",
}

if day < 1:
    print(f"The integer you chose is smaller than 1!: {day}")
elif day == 1:
    print(f"You chose the 1st day: {weekday[day]}")
elif day == 2:
    print(f"You chose the 2nd day: {weekday[day]}")
elif day == 3:
    print(f"You chose the 3rd day: {weekday[day]}")
elif day <= 7:
    print(f"You chose the {day}th day: {weekday[day]}")
else:
    print(f"The integer you chose is bigger than 7!: {day}")
