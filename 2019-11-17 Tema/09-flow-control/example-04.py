print(range(3))  # only 1 argument - stop
print(list(range(0, 3)))  # start and stop
print(list(range(1, 10, 2)))  # start, stop, step
print(list(range(0, -10, -1)))  # start, stop, step
print(range(5) == range(0, 5) == range(0, 5, 1))

for iteration in range(3):
    print(f"Iteration #{iteration}")
