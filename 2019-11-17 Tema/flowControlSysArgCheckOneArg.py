import sys
if len(sys.argv) != 2:
    print("Error! Application does not accept more than one argument!")
    sys.exit(1)
print(f"Inputted name is: {sys.argv[1]}")