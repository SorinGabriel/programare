class Node:
    red = 1
    black = 0

    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data
        self.color = self.black

    def insert(self, data):
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.left = Node(data)
                else:
                    self.left.insert(data)
            elif data > self.data:
                if self.right is None:
                    self.right = Node(data)
                else:
                    self.right.insert(data)
        else:
            self.data = data

    def traverse(self, lkval):
        if lkval < self.data:
            if self.left is not None:
                for v in self.left.traverse(lkval):
                    yield v
        elif lkval > self.data:
            if self.right is not None:
                for v in self.right.traverse(lkval):
                    yield v
        else:
            yield lkval

    def findval(self, lkval):
        func = self.traverse(lkval)
        try:
            while True:
                if next(func) == lkval:
                    return True
        except StopIteration:
            return False

    def PrintTree(self):
        if self.left:
            self.left.PrintTree()
        print(self.data)
        if self.right:
            self.right.PrintTree()


root = Node(12)
root.insert(6)
root.insert(14)
root.insert(3)
print(root.findval(7))
print(root.findval(14))
root.insert(5)
print(root.findval(6))
print(root.findval(5))
