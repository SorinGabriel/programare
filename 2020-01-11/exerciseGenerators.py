def my_fnct():
    yield "Tata"
    yield 30
    yield True


def my_fnct2():
    return ["Tata", 30, True]


a = my_fnct()
b = iter(my_fnct2())
"""
while True:
    print(next(a))

while True:
    print(next(b))
"""

def my_range(start, stop, step=1):
    items = []
    position = start
    while position < stop:
        print(position)
        items.append(position)
        #yield position
        position += step
    return items


#print([num for num in my_range(100,500, step = 100)])
a = my_range(100, 500, 100)
b = my_range(100, 500, 100)
print(a, b)

def my_range2(start, stop, step=1):
    position = start
    while position < stop:
        print(position)
        yield position
        position += step

#print([num for num in my_range(100,500, step = 100)])
c = list(my_range2(100, 500, 100))
d = list(my_range2(100, 500, 100))
print(c, d)