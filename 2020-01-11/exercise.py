"""
Exercise 2 - lambdas - p47 Python Advanced
"""
fruit = ['apples', 'oranges', 'grapes']
result = dict(map(lambda x: (x, len(x)), fruit))
print(result)

"""
p53 Python Advanced - function which returns the car model with lowest price, provide the brand to seek into
"""
from functools import reduce
class Car:
    def __init__(self, make, model, price):
        self.make = make
        self.model = model
        self.price = price

cars = [
    Car("Ford", "Anglia", 300.0),
    Car("Ford", "Cortina", 700.0),
    Car("Alfa Romeo", "Stradale 33", 190.0),
    Car("Alfa Romeo", "Giulia", 500.0),
    Car("Citroën", "2CV", 75.0),
    Car("Citroën", "Dyane", 105.0),
    Car("Citroën", "2CVV", 65.0),
]
# Let's find the price of the cheapest Citroën on the list
c = reduce(
min, map(lambda car: car.price, filter(lambda car: car.make == "Citroën", cars))
)

print(f"The cheapest Citroën costs: {c}")
import sys
def queryModelWithLowestPrice(cars = [], make = ""):
    if len(cars) < 1:
        return "Error! No cars provided!"

    if make == "":
        return "Error! Make not specified!"

    makeFilteredCars = {}
    min = sys.float_info.max
    if make != "":
        for car in cars:
            if car.make == make:
                makeFilteredCars[car.price] = car.model
                if car.price < min:
                    min = car.price
    return makeFilteredCars[min]
    #makeFilteredCars = {car.price: car.model for car in cars if car.make == make}
    #return makeFilteredCars[min(makeFilteredCars)]

# Query the car model with the lowest price of the Ford make from cars list
print(queryModelWithLowestPrice(cars, "Ford"))
# Query the car model with the lowest price within all makes from cars list
print(queryModelWithLowestPrice(cars))


liste = [
            [
                [
                    [1, 2, 3, 4],
                    [2, 8],
                    [3, 5]
                ]
            ],
            [5, 2]
        ]

print(f"{liste[0][0][1][1]}")


liste = [
            [
                {"1": 3},
                {"4": 5},
                {"7": 2}
            ],
            [
                {"2": 3},
                {"1": 0}
            ],
            (
                {"Alina": 10}
            ),
            {
                "2": (
                    [
                        False,
                        False,
                        True
                    ]
                )
            }
]

testList = [{"1": 7, "x": 8, "100": 2}, {"a": 132}, {"acb": 1111}]
dictionary = {}
for compDict in testList:
    dictionary.update(compDict)

print("Key with maximum number is:")
print(max(dictionary, key=dictionary.get))


