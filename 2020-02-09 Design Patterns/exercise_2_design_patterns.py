import logging
import time

l = logging.getLogger()
l.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
l.addHandler(console_handler)

class Logger:

    @classmethod
    def log(cls, msg):
        l.info(msg)


class WithLevel:
    def __init__(self, log_level, logger):
        self._log_level = log_level
        self._logger = logger


    def log(self, msg):
        self._logger.log(self._log_level + " " + msg)


class WithTimestamp:
    def __init__(self, logger):
        self._logger = logger

    def log(self, msg):
        self._logger.log(str(time.time()) + " " + msg)



if __name__ == "__main__":
    timestamp = WithTimestamp(Logger())
    timestamp.log("test")

    timestampLevel = WithLevel("ERROR", WithTimestamp(Logger()))
    timestampLevel.log("test 2")