"""
1. Create a new list ["Fido", "Duke"] and assign is to a variable dogs
"""
print("1. Create a new list [\"Fido\", \"Duke\"] and assign is to a variable dogs")
dogs = ["Fido", "Duke"]
print(f"dogs variabile: {dogs}")
print()
"""
2. Create a new binding of existing list to a new name: my_dogs
"""
print("2. Create a new binding of existing list to a new name: my_dogs")
new_dogs = dogs
print(f"New binding my_dogs: {new_dogs}")
print()
"""
3. Append a new dog to the list using any of the names
"""
print("3. Append a new dog to the list using any of the names")
dogs.append("New Name")
print(f"list updated: {dogs}")
print()
"""
4. Print the list using both names. Are they the same?
"""
print("4. Print the list using both names. Are they the same?")
print(f"updated listed calling with new_dogs (they are the same because they are refferenes to single list: {new_dogs}")
print()