"""
1. Create a new dictionary. Print its id.
"""
print("1. Create a new dictionary. Print its id.")
demoDict = {'a': 1, 'd': 100}
print(f"demoDict id: {id(demoDict)}")
print()
"""
2. Update the dictionary, check its id. Is it the same?
"""
print("2. Update the dictionary, check its id. Is it the same?")
demoDict['ff'] = 1000
print(f"Updated demo dict  {demoDict} id: {id(demoDict)}")
demoDict['ffa'] = 1000
demoDict['ffb'] = 1000
demoDict['ffc'] = 1000
demoDict['ffd'] = 1000
print(f"Updated demo dict {demoDict} id: {id(demoDict)}")
print()
"""
3. Create a new string. Print its id.
"""
print("3. Create a new string. Print its id.")
testString = "asfgasdasadad"
print(f"testString id: {id(testString)}")
print()
"""
4. Add another string to it using + operator. Check new string's id.
"""
print("4. Add another string to it using + operator. Check new string's id.")
testString += "aaaaaaaaaa"
print(f"Updated testString id: {id(testString)}")
print()
"""
5. extracurricular exercise - for the curious
• create variable a = 5
• create variable b = 2 + 3
• create variable c = 300
• create variable d = 3 * 100
• Compare IDs of a, b, c and d. What are they? Why is that? Ask a tutor to explain.
"""
print("5. extracurricular exercise - for the curious")
print("• create variable a = 5")
a = 5
print("• create variable b = 2 + 3")
b = 2 + 3
print("• create variable c = 300")
c = 300
print("• create variable d = 3 * 100")
d = 3 * 100
print("• Compare IDs of a, b, c and d. What are they? Why is that? Ask a tutor to explain.")
print(f"id of a: {id(a)}")
print(f"id of b: {id(b)}")
print(f"id of c: {id(c)}")
print(f"id of d: {id(d)}")
print()

