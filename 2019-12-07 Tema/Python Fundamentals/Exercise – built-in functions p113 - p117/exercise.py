"""
1. Using abs(), calculate the absolute value of 5 and -7
"""
print("1. Using abs(), calculate the absolute value of 5 and -7")
print(f"Absolute value of 5: {abs(5)}")
print(f"Absolute value of -7: {abs(-7)}")
print()
"""
2. Using all(), check whether all the expressions are true: True, 1 == 1, "x" in []
"""
print("2. Using all(), check whether all the expressions are true: True, 1 == 1, \"x\" in []")
print(f"All the expressions (True, 1 == 1, \"x\" in []) are true: {all([True, 1 == 1, 'x' in []])}")
print()
"""
3. Using any(), check whether any of the expressions are true: False, None, "a" in "abc"
"""
print('3. Using any(), check whether any of the expressions are true: False, None, "a" in "abc"')
print(f"Any the expressions ( False, None, \"a\" in \"abc\") are true: {any([False, None, 'a' in 'abc'])}")
print()
"""
4. Convert "Ünicöde" to ASCII using ascii()
"""
print('4. Convert "Ünicöde" to ASCII using ascii()')
print(f'"Ünicöde" converted to ASCII: {ascii("Ünicöde")}')
print()
"""
5. Find out the binary representation of 19 using bin()
"""
print('5. Find out the binary representation of 19 using bin()')
print(f'Binary representation of 19 is: {bin(19)}')
print()
"""
6. bool() covered in the next chapter
"""
print("6. bool() covered in the next chapter")
print("Testing some parameters for bool() function: ")
print(f"0 parameter: {bool(0)}")
print(f"[] parameter: {bool([])}")
print("{}" + f" parameter: {bool({})}")
print(f"1 parameter: {bool(1)}")
print()
"""
7. breakpoint() starts a debugger session at the call site.
"""
print("7. breakpoint() starts a debugger session at the call site.")
# print(f"Testing breakpoint() call {breakpoint()}")
print()
"""
8. Using bytearray(), convert a string to byte array.
"""
print("8. Using bytearray(), convert a string to byte array.")
string = 'aBcDeFgHiJ'
utf8 = 'utf-8'
utf16 = 'utf-16'
print(f"converting '{string}' to byte array with {utf8} encoding using bytearray(): {bytearray(string, utf8)}")
print(f"converting '{string}' to byte array with {utf16} encoding using bytearray(): {bytearray(string, utf16)}")
print('asdsadadsadsd')
"""
9. Using bytes(), convert a string to bytes literal.
"""
print("9. Using bytes(), convert a string to bytes literal.")
string = 'aBcDeFgHiJ'
utf8 = 'utf-8'
utf16 = 'utf-16'
print(f"converting '{string}' to byte literal with {utf8} encoding using bytes(): {bytes(string, utf8)}")
print(f"converting '{string}' to byte literal with {utf16} encoding using bytes(): {bytes(string, utf16)}")
print()
"""
10. Using callable(), check whether bin() built-in function is callable.
"""
print("10. Using callable(), check whether bin() built-in function is callable.")
print(f"bin() built-in function is callable: {callable(bin)}")
print()
"""
11. Using chr(), convert Unicode code 97 to a character.
"""
print("11. Using chr(), convert Unicode code 97 to a character.")
print(f"Converting Unicode code 97 to a character using chr() built-in function: {chr(97)}")
print()

"""
12. @classmethod is an advanced way of providing class constructors.
"""
print(" @classmethod is an advanced way of providing class constructors.")


class Person:
    totalInstances = 0

    def __init__(self):
        Person.totalInstances += 1

    @classmethod
    def getTotalInstances(cls):
        print(f"Total amount of instances is: {cls.totalInstances}")


print("Call getTotalInstances() method of Person which hash @classmethod decorator applied:")
print(f"No instance defined")
Person.getTotalInstances()
person1 = Person()
print(f"One instance defined")
Person.getTotalInstances()
person2 = Person()
print(f"Two instances defined")
Person.getTotalInstances()


class Calculator:

    @staticmethod
    def multiply(x, y):
        return x * y;


print(
    f"With @staticmethod decorator, I don't have to pass self or cls as first argument and can call the method using class' name: {Calculator.multiply(5, 6)}")
print()
"""
13. compile is an advanced function used when parsing Python code.
"""
print("13. compile is an advanced function used when parsing Python code.")
print("Using compile() built-in function to compile a single line of code:")
toCompile = compile('print("100 string")', 'test', 'eval')
exec(toCompile)
print("Using compile() built-in function to compile two lines of code:")
print()
toCompile = compile('print("string of first line of code")\nprint("string of the second line of code")', 'test', 'exec')
exec(toCompile)
print()
"""
14. Use complex() to create a complex number 1+2j
"""
print("14. Use complex() to create a complex number 1+2j")
print(f"Using complex() built-in function in order to create the complex number 1+2j : {complex(1, 2)}")
print()
"""
15. delattr() is used to remove object's attribute.
"""
print("15. delattr() is used to remove object's attribute.")


class PropertiesStore:
    property1 = 1
    property2 = 2


pSInstance = PropertiesStore()
print(f"Accessing property before calling delattr() on it: {pSInstance.property1}")
delattr(PropertiesStore, 'property1')

# This one raises error as I am trying to call a property that does not exist as it has been deleted using delattr()
# built in function

# print(f"Accessing property after calling delattr() on it: {pSInstance.property1}")
print()
"""
16. Pass keyword arguments to dict() to create a new dictionary.
"""
print("16. Pass keyword arguments to dict() to create a new dictionary.")
print(f"Creating a dictionary using dict() built in function {dict(name='Test', age='Unknown', gender='M')}")
print()
"""
17. dir() will be covered in chapter 18
"""
print("17. dir() will be covered in chapter 18")


class Coordinates:
    x = 0
    y = 0
    z = 0

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def prettyPrint(self):
        print(f"({self.x}, {self.y}, {self.z})")


coordinate1 = Coordinates(1, 5, 2)
coordinate1.prettyPrint()
print(f"Displaying the content of Coordinates instance using dir() built in function: {dir(coordinate1)}")
print()
"""
18. Using divmod(a, b), find out what is the quotient and remainder of dividing 15 by 2
"""
print("18. Using divmod(a, b), find out what is the quotient and remainder of dividing 15 by 2")
print(f"Quotient and remainder of diving 15 by 2 gained using divmod(a, b) built-in function: {divmod(15, 2)}")
print()
"""
19. Using enumerate(), print indices and values of ["apple", "banana", "orange"]
"""
print("19. Using enumerate(), print indices and values of [\"apple\", \"banana\", \"orange\"]")
print(f"Indices and values returned using enumerate() on  [\"apple\", \"banana\", \"orange\"]:")
print(list(enumerate(['apple', 'banana', 'orange'])))
print()
"""
20. Using eval() with x = 1, evaluate x+3
"""
print("20. Using eval() with x = 1, evaluate x+3")
print(f"Evaluating x+3 using eval() with x = 1 under globals: {eval('x+3', {'x': 1})}")
print()
"""
21. Using exec(), execute the script a = 5; print(f"result = {a}")
"""
print("21. Using exec(), execute the script a = 5; print(f\"result = {a}\")")
print("Executing a = 5; print(f\"result = {a}\")" + "using exec():")
exec('a = 5\nprint(f"result = {a}")')
print()
"""
22. Define function is_even(num) which returns True for even and False for odd numbers.
Use it in filter() function, to get a list of even numbers between 0 and 15.
"""
print("22. Define function is_even(num) which returns True for even and False for odd numbers.")
print("Use it in filter() function, to get a list of even numbers between 0 and 15.")
print(f"Getting a list of event numbers between 0 and 15 using is_even(num): ")


def is_even(num):
    return not num % 2


evenNumbers = filter(is_even, range(0, 16))
for evenNumber in evenNumbers:
    print(evenNumber)
print()
"""
23. float() will be covered in the next chapter.
"""
print("23. float() will be covered in the next chapter.")
print(f"Turning '-10.14' string to float using float(): {float('-10.14')}")
print(f"Turning -10.14 float to float using float(): {float(-10.14)}")
print(f"Turning 10 int to float using float(): {float(10)}")
print(f"Calling float() without param returns 0.0 float: {float()}")
print()
"""
24. Use format() to print 3.1415926 to the 2nd decimal place. Formatting matches the one used in f-strings.
"""
print("24. Use format() to print 3.1415926 to the 2nd decimal place. Formatting matches the one used in f-strings.")
print(f"Using format to print 3.1415926 to the 2nd decimal place: {'{0:.3g}'.format(3.1415926)}")
print()
"""
25. frozenset() will be covered in the next chapter.
"""
print("25. frozenset() will be covered in the next chapter.")
print(f"frozenset() is an immutable version of Python sets: {frozenset([1, 5, 2, 5, 7, 4])}")
print()
"""
26. getattr() is used to retrieve object's attribute.
"""
print("26. getattr() is used to retrieve object's attribute.")


class Engine:

    def __init__(self, mass, traction, overallLoss):
        self.mass = mass
        self.traction = traction
        self.overallLoss = overallLoss


engine1 = Engine(600, 5400, .98)

print(f"Getting engine instance mass attribute value: {getattr(engine1, 'mass')}")
print()
"""
27. globals() returns a dictionary representing the current global symbol table.
"""
print("27. globals() returns a dictionary representing the current global symbol table.")
print(f"Printing globals() output: {globals()}")
print()
"""
28. hasattr() checks whether given string is a name of one of the object's attributes.
"""
print("28. hasattr() checks whether given string is a name of one of the object's attributes.")
print(f"Engine instance has mass attribute: {hasattr(engine1, 'mass')}")
print(f"Engine instance has torque attribute: {hasattr(engine1, 'torque')}")
print()
"""
29. hash() returns a hash of the object (if it has one).
"""
print("29. hash() returns a hash of the object (if it has one).")
engine2 = Engine(600, 5400, .98)
engine3 = Engine(700, 10000, .93)
print(f"Engine 1 instance hash is: {hash(engine1)}")
print(f"Engine 2 instance (with same properties as engine 1) hash is: {hash(engine2)}")
print(f"Engine 3 instance hash is: {hash(engine3)}")
print()
"""
30. help() invokes a built-in help system. Check it out by using help(print)
"""
print("30. help() invokes a built-in help system. Check it out by using help(print)")
print(f"Trying out help(print): {help(print)}")
print()
"""
31. Find out the hexadecimal representation of 19 using hex()
"""
print("31. Find out the hexadecimal representation of 19 using hex()")
print(f"Getting hexadecimal represnetation of 19 using hex() built-in function: {hex(19)}")
print()
"""
32. id() will be covered in chapter 18
"""
print("32. id() will be covered in chapter 18")

tuple1 = ('apples', 'trees')
tuple2 = ('apples', 'trees')
tuple3 = ('engines', 'test chambers')
print(f"Calling id() with tuple1 as param: {id(tuple1)}")
print(f"Calling id() with tuple2 (same values as tuple1) as param: {id(tuple2)}")
print(f"Calling id() with tuple3 as param: {id(tuple3)}")
print()

"""
33. input() has been covered in chapter 11
"""
print("3. input() has been covered in chapter 11")
# x = input(f"Please enter an input:")
# print(f"Inputted value: {x}")
print()
"""
34. int() will be covered in the next chapter
"""
print("34. int() will be covered in the next chapter")
print("Testing few int calls:")
print(f"int() call with int param: {int(5)}")
print(f"int() call with float param: {int(3.8)}")
print(f"int() call with string number param: {int('7')}")
print(f"int() call with no param: {int()}")
print()
"""
35. isinstance() will be covered in chapter 18
"""
print("35. isinstance() will be covered in chapter 18")
print("Using isinstance() built-in function: ")
print(f"Test if number is an int: {isinstance(3, int)}")
print(
    f"Test if a string 'Test' is an instance of the range of parameters given as type: {isinstance('Test', (float, int, str, list, dict, tuple))}")
print(f"Test if engine1 object is instance of Engine class: {isinstance(engine1, Engine)}")
print()
"""
36. issubclass() will be covered in the next course stages
"""
print("36. issubclass() will be covered in the next course stages")


class EngineContainer(Engine):
    engine = Engine


print(f"Check if EngineContainer is subclass of Engine: {issubclass(EngineContainer, Engine)}")
print(f"Check if Engine is subclass of EngineContainer: {issubclass(Engine, EngineContainer)}")

print()
"""
37. iter() creates an iterator from either a collection or a callable
"""
print("37. iter() creates an iterator from either a collection or a callable")
# Creating iterable using iter() built-in function
iterable = iter(["A", "B", "C"])

print(f"Next value from iterable: {next(iterable)}")
print(f"Next value from iterable: {next(iterable)}")
print(f"Next value from iterable: {next(iterable)}")
# print(f"Out of stack: {next(iterable)}") (throws error)
print()
"""
38. Check the length of a "word" using len()
"""
print("38. Check the length of a 'word' using len()")
print(f"Checking the length of 'word' using len() built in function: {len('word')}")

print()
"""
39. list() will be covered in the next chapter
"""
print("39. list() will be covered in the next chapter")
print("You can create a list using list() from strings, collections or from custom iterator object:")
print(f"List generated from string: {list('abcdefghj')}")
print(f"List generated from tuple: {list(('a', 'b', 'c', 'd', 'e'))}")
print(f"List generated from set: {list(set(['a', 'b', 'c', 'd', 'e']))}")
print(f"List generated from dictionary: {list({'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4})}")
print(f"List generated from list: {list(['a', 'b', 'c', 'd', 'e'])}")


class PowTwo:
    def __init__(self, max):
        self.max = max

    def __iter__(self):
        self.num = 0
        return self

    def __next__(self):
        if self.num >= self.max:
            raise StopIteration
        result = 2 ** self.num
        self.num += 1
        return result


powTwo = PowTwo(5)
powTwoIter = iter(powTwo)

print(f"List generated from list: {list(powTwoIter)}")

print()
"""
40. locals() can update and return a dictionary representing the current local symbol table
"""
print("40. locals() can update and return a dictionary representing the current local symbol table")
print(f"Calling locals() built-in function: {locals()}")

print()
"""
41. Define a function add_three(num) which adds three to any integer. Use map() to apply it to every element of list: [1,3, 4, 10, 22]
"""
print(
    "41. Define a function add_three(num) which adds three to any integer. Use map() to apply it to every element of list: [1,3, 4, 10, 22]")


def add_three(num):
    return num + 3


print(
    f"Execute add_three(num) function on every item from the [1,3, 4, 10, 22] iterable using map() built-in function: {list(map(add_three, [1, 3, 4, 10, 22]))}")

print()
"""
42. Using max(), find the biggest number in range(100, 20, -3)
"""
print("42. Using max(), find the biggest number in range(100, 20, -3)")
print(f"Finding the biggest number in range(100, 20, -3) using max(): {max(range(100, 20, -3))}")

print()
"""
43. memoryview() creates a "memory view" object
"""
print("43. memoryview() creates a 'memory view' object")
x = memoryview(b"Hello")

print(x)
print(f"Memoryview instance: {x}")

# return the Unicode of the first character
print(x[0])

# return the Unicode of the second character
print(x[1])

print()
"""
44. Using min() find the smallest number in range(100, 20, -3)
"""
print("44. Using min() find the smallest number in range(100, 20, -3)")
print(f"Finding the smallest number in range(100, 20, -3) using min(): {min(range(100, 20, -3))}")

print()
"""
45. Using next(), retrieve the next object from iter([4, 5, 6])
"""
print("45. Using next(), retrieve the next object from iter([4, 5, 6])")
iterable = iter([4, 5, 6])
print(f"Retrieving the next object from iter([4,5,6]): {next(iterable)}")

print()
"""
46. object() returns a new, featureless object. More on that in chapter 18.
"""
print("46. object() returns a new, featureless object. More on that in chapter 18.")
print(f"Basic object: {object()}")

print()
"""
47. Find out the octal representation of 19 using oct()
"""
print("47. Find out the octal representation of 19 using oct()")
print(f"Getting octal representation of 19 using oct(): {oct(19)}")

print()
"""
48. open() will be covered in chapter
"""
print("48. open() will be covered in chapter")
file = open('demo file.txt', 'r')
print(f"Opening a file on read mode using open(): {file}")
print(f"Reading demo file content: {file.read()}")
"""
49. Using ord(), find out the Unicode code of the "C" character.
"""
print("49. Using ord(), find out the Unicode code of the 'C' character.")
print(f"Getting the Unicode code of the 'C' character using ord(): {ord('C')}")
print()
"""
50. Using pow(), calculate 5 to 4th power
"""
print("50. Using pow(), calculate 5 to 4th power")
print(f"Calculating 5**4 using pow(): {pow(5, 4)}")
print()
"""
51. print() has been covered in chapter 9
"""
print("51. print() has been covered in chapter 9")
print(f"This and most console text appears due to print() function")
print()
"""
52. property() will be covered in chapter 18.
"""
print("52. property() will be covered in chapter 18.")
print("Defined EngineEnhanced class with get, set and del method and attached them to mass property")


class EngineEnhanced:

    def __init__(self, mass):
        self.setMass(mass)

    def getMass(self):
        print("Getting mass")
        return self._mass

    def setMass(self, newMass):
        print(f"Setting mass to {newMass} kg")
        self._mass = newMass

    def delMass(self):
        print(f"Deleting mass")
        del self._mass

    def __repr__(self):
        return f"This engine has a mass of {self._mass} kg"

    mass = property(getMass, setMass, delMass, 'Mass property')


print("Testing propery() function")
print(" Default get, set and del behaviors will be overridden by the defined ones within property() call")
print("Defined methods will also print something")
engineI = EngineEnhanced(900)
print(engineI.mass)
engineI.mass = 2700
del engineI.mass
engineI.mass = 3400
print()
"""
53. range() has been covered in chapter 12.
"""
print("53. range() has been covered in chapter 12.")
print(f"Range from 3 up to 99 with a step of 2: {range(3, 99, 2)}")
print()
"""
54. repr() returns a string containing a printable representation of the object. It will be partially covered in chapter 18.
"""
print("54. repr() returns a string containing a printable representation of the object. It will be partially covered in chapter 18.")
print(f"Testing repr() call on EnhancedEngine instance declared above (takes the output from class' __repr__() method: {repr(engineI)}")
print()
"""
55. Using reversed(), print a reversed list: ["a", "l", "p", "h", "a"]
"""
print("55. Using reversed(), print a reversed list: ['a', 'l', 'p', 'h', 'a']")
print(f"Printing a reversed lis of ['a', 'l', 'p', 'h', 'a'] using reversed() built-in function: {reversed(['a', 'l', 'p', 'h', 'a'])}")
print()
"""
56. Using round(), round 2.11843 to the 2nd decimal place
"""
print("56. Using round(), round 2.11843 to the 2nd decimal place")
print(f"Rounding 2.11843 to the 2nd decimal place using round() built-in function: {round(2.11843, 2)}")
print()
"""
57. set() will be covered in the next chapter.
"""
print("57. set() will be covered in the next chapter.")
print(f"Initiating a set collection: {set('ABCDEFG')}")
print()
"""
58. setattr() is used to add attributes to objects.
"""
print("58. setattr() is used to add attributes to objects.")
print("Adding velocity attribute to the instance EngineEnhanced:")
setattr(engineI, 'velocity', 250)
print(f"Velocity is: {engineI.velocity} km/h")
print(f"")
print()
"""
59. Use slice() to manually create a slice object with start=0, stop=3. Then use it on [9, 8, 7, 6, 5, 4].
"""
print("59. Use slice() to manually create a slice object with start=0, stop=3. Then use it on [9, 8, 7, 6, 5, 4].")
sliceOb = slice(0, 3)
print(f"Created a slice object start=0, stop= 3: {sliceOb}")
demoList = [9, 8, 7, 6, 5, 4]
print(f"List: {demoList =}")
print(f"Applying slice on [9, 8, 7, 6, 5, 4] list: {demoList[sliceOb]}")
print()
"""
60. Use sorted to get ["x", "g", "d", "c", "t", "a"] sorted alphabetically.
"""
print("60. Use sorted to get ['x', 'g', 'd', 'c', 't', 'a'] sorted alphabetically.")
print(f"Sorting ['x', 'g', 'd', 'c', 't', 'a'] alphabetically using sorted(): {sorted(['x', 'g', 'd', 'c', 't', 'a'])}")
print()
"""
61. staticmethod() will be covered in chapter 18.
"""
print("61. staticmethod() will be covered in chapter 18.")


class CustomCalculator:
    def relativeAdd(x, y):
        return x + y + x * y


CustomCalculator.relativeAdd = staticmethod(CustomCalculator.relativeAdd)

print(f"Calling static method set with staticmethod(): {CustomCalculator.relativeAdd(5, 4)}")
print()
"""
62. str() will be covered in the next chapter.
"""
print("62. str() will be covered in the next chapter.")
print(f"Converting 12.53 number to a string using str(): {str(12.53)}")
print()
"""
63. Use sum() to calculate a sum of all elements in [1.3, 15.7, 993.0, 2.1343355]
"""
print("63. Use sum() to calculate a sum of all elements in [1.3, 15.7, 993.0, 2.1343355]")
print(f"Calculating the sum of all elements within [1.3, 15.7, 993.0, 2.1343355] list using sum(): {sum([1.3, 15.7, 993.0, 2.1343355])}")
print()
"""
64. super() will be covered in the next sections of the course.
"""
print("64. super() will be covered in the next sections of the course.")


class CustomCalculatorEnhanced(CustomCalculator):
    def relativeAdd(x, y):
        return super(CustomCalculatorEnhanced, CustomCalculatorEnhanced).relativeAdd(x, y) + x ** y


CustomCalculatorEnhanced.relativeAdd = staticmethod(CustomCalculatorEnhanced.relativeAdd)


print(f"Testing super() functionality: {CustomCalculatorEnhanced.relativeAdd(5, 4)}")
print()
"""
65. tuple() will be covered in the next chapter.
"""
print("65. tuple() will be covered in the next chapter.")
print(f"Create a tuple collection from string(): {tuple('adadadada')}")
print()
"""
66. type() returns the type of the object or constructs new type objects.
"""
print("66. type() returns the type of the object or constructs new type objects.")
print(f"Getting the type of EngineEnhanced instance: {type(engineI)}")
print()
"""
67. vars() returns __dict__ attribute of an object. __dict__ will be discussed in chapter 18.
"""
print("67. vars() returns __dict__ attribute of an object. __dict__ will be discussed in chapter 18.")
print(f"Calling vars() with EngineEnhanced parameter: {vars(EngineEnhanced)}")
print()
"""
68. zip() has been discussed in chapter 12
"""
print("68. zip() has been discussed in chapter 12")
print(f"trying out a zip call on a list and a tuple of same size: {dict(zip(['a', 'b', 'c'], (1, 2, 4)))}")
print(f"trying out a zip call on a list and a tuple of different size: {dict(zip(['a', 'b', 'c', 'd'], (1, 2, 4)))}")
print()
"""
69. __import__ will be discussed in chapter 17.
"""
print("69. __import__ will be discussed in chapter 17.")
mathematics = __import__('math', globals(), locals(), [], 0)
print(f"Imported math module under and stored it in mathematics variable and with it I will get the value of PI: {mathematics.pi}")
print()
