# Figure positions
positions = {
    "A1": "Black Rook",
    "D8": "White Knight",
    "B2": "White Pawn",
    "B3": "White Pawn",
    "F5": "Black King",
    "C1": "Black Knight",
    "D2": "White Queen",
    "E4": "White Bishop",
}
# Figure name to figure symbol
figures = {
    "Black King": "♛",
    "Black Queen": "♚",
    "Black Rook": "♜",
    "Black Bishop": "♝",
    "Black Knight": "♞",
    "Black Pawn": "♟",
    "White King": "♕",
    "White Queen": "♔",
    "White Rook": "♖",
    "White Bishop": "♗",
    "White Knight": "♘",
    "White Pawn": "♙",
}

# Convert figure names to figure symbols, so the chessboard is displayed right.
symbol_positions = {
    # position: figure_symbol
    position: figures[positions[position]]
    # for ..., ... in ...
    for position in positions.keys()
}

empty_square = "□"
for row in range(9, 0, -1):
    for column in "ABCDEFGH":
        coordinates = f"{column}{row}"
        if coordinates in symbol_positions:
            print(symbol_positions[coordinates], end=" ")
        else:
            print(empty_square, end=" ")
    print()
