"""
Write a script which prompts user for input in a loop and takes an action based on it:
• dir <name> creates a new directory
• file <name> creates a new empty file
• cd <path> changes directories using given path and prints current path after change
• ls lists contents of current directory
• q quits the script
"""
import os

if __name__ == "__main__":
    while True:
        command = input("Please enter a command or enter 'quit' to exit the loop: ")
        if command == "":
            print("Command can not be empty!")
            continue

        arguments = command.split()

        if arguments[0] == "dir":
            if arguments[1] != '':
                print("test")
            else:
                print("Folder must have a name")
            continue
        elif arguments[0] == "file":

            continue
        elif arguments[0] == "cd":

            continue
        elif arguments[0] == "ls":

            continue
        elif arguments[0] == "q":
            print(f"Quitting loop")
            break
        else:
            print(f"Unknown command")
