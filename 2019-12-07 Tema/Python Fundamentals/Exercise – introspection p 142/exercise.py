"""
1. Given a = 3, use dir() to discover attributes of a
"""
print("1. Given a = 3, use dir() to discover attributes of a")
a = 3
print(f"{dir(a)}")
print()
"""
2. Define an empty class class Empty: pass and use dir() on it.
"""
print("")


class Empty:
    pass


print(f"{dir(Empty)}")
print()
"""
3. Import one of standard library modules: time and use dir() on it.
"""

print("3. Import one of standard library modules: time and use dir() on it.")

import time

print(f"{dir(time)}")
print()
"""
4. hasattr(<object>, <attribute_name>) is another method used for introspection. Use hasattr() to
check whether the time module has an attribute called localtime.
"""
print("4. hasattr(<object>, <attribute_name>) is another method used for introspection. Use hasattr() to check whether the time module has an attribute called localtime.")
print(f"{hasattr(time, 'localtime')}")
print()
