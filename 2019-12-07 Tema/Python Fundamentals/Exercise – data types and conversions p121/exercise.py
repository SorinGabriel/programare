"""
1.Use bool() to
1. convert 0, 1, and 2 into boolean values
2. convert string False into boolean value. What is the result? Why?
"""
print("1.Use bool() to")
print("1. convert 0, 1, and 2 into boolean values")
print(f"Converting 0 to boolean value using bool() function: {bool(0)}")
print(f"Converting 1 to boolean value using bool() function: {bool(1)}")
print(f"Converting 2 to boolean value using bool() function: {bool(2)}")

print()
print("2. convert string False into boolean value. What is the result? Why?")
print(
    f"Converting string False into boolean value using bool() function will give True becuase False string is non empty string hence true according to truth value testing: {bool('False')}")
print()
"""
2.Use float() to
1. convert string 123.456 into a floating point number
2. convert integer 123 into a floating point number
3. parse "inf" to get the closest thing to infinity in Python
"""
print("2.Use float() to")
print("1. convert string 123.456 into a floating point number")
print(f"{float('123.456')}")
print()
print("2. convert integer 123 into a floating point number")
print(f"{float(123)}")
print()
print("3. parse \"inf\" to get the closest thing to infinity in Python")
print(f"{float('inf')}")
print()
print()
"""
3.Use int() to
1. convert string -5 into an integer
2. convert octal "0o123" into an integer, using optional parameter base=8
3. convert float 123.7 into an integer, losing places after decimal point
"""
print("3.Use int() to")
print("1. convert string -5 into an integer")
print(f"{int('-5')}")
print()
print("2. convert octal \"0o123\" into an integer, using optional parameter base=8")
print(f"{int('0o123', base=8)}")
print()
print("3. convert float 123.7 into an integer, losing places after decimal point")
print(f"{int(123.7)}")
print()
"""
4.Use dict() to
1. convert the result of zip(["a", "b"], [1, 2]) into a dictionary
"""
print("4.Use dict() to ")
print("1. convert the result of zip([\"a\", \"b\"], [1, 2]) into a dictionary")
print(f'{dict(zip(["a", "b"], [1, 2]))}')
print()
"""
5.Use tuple() to
1. convert range(10) into a tuple
"""
print("5.Use tuple() to")
print("1. convert range(10) into a tuple")
print(f"{tuple(range(10))}")
print()
"""
6.Use set() to
1. convert the list [1, 2, 3, 3, 4, 2, 5] into a set. What happened to repeated values?
"""
print("6.Use set() to")
print("1. convert the list [1, 2, 3, 3, 4, 2, 5] into a set. What happened to repeated values?")
print(f"Repeated values dissapear: {set([1, 2, 3, 3, 4, 2, 5])}")
print()
"""
7.Use list() to
1. convert range(10) into a list
2. convert tuple(range(2)) into a list
"""
print("7.Use list() to")
print("1. convert range(10) into a list")
print(f"{list(range(10))}")
print()
print("2. convert tuple(range(2)) into a list")
print(f"{list(tuple(range(2)))}")
print()
"""
8.Use frozenset() to
1. convert the set {1, 2, 3} into an immutable frozenset. What happens when you call .add(4) on the frozenset?
"""
print("8.Use frozenset() to")
print("1. convert the set {1, 2, 3} into an immutable frozenset. What happens when you call .add(4) on the frozenset?")
frSet = frozenset({1, 2, 3})
print(f"{frSet}")
# Trying to call .add(4) to frozenset. Can not add, object has no attribute 'add' error
# frSet.add(4)
# print(f"New frozenset: {frSet}")
print()
"""
9.Use str() to convert … to a string
1. 5
2. 123.4
3. [1, 2, 3]
4. {"key": "value"}
"""
print("9.Use str() to convert … to a string")
print("1. 5")
print(f"Converting to string with str() call: {str('1. 5')}")
print()
print("2. 123.4")
print(f"Converting to string with str() call: {str('2. 123.4')}")
print()
print("3. [1, 2, 3]")
print(f"Converting to string with str() call:  {str([1, 2, 3])}")
print()
print("4. {\"key\": \"value\"}")
print(f"Converting to string with str() call: {str({'key': 'value'})}")
print()
