"""
1. Given a list containing a year of birth of 7 people, calculate their current age:
year_of_birth = [1915, 1937, 1964, 1965, 1993, 2001, 2009]
"""
print("1. Given a list containing a year of birth of 7 people, calculate their current age:year_of_birth = [1915, 1937, 1964, 1965, 1993, 2001, 2009]")

# Get the current year and print it
import datetime
currentDatetime = datetime.datetime.now()
currentYear = currentDatetime.year
print(f"Current year: {currentYear}")
# Set the years of birth from the exercise
year_of_birth = [1915, 1937, 1964, 1965, 1993, 2001, 2009]
# Print the exercise's requested result
print(f"Result:{[currentYear - yearOfBirth for yearOfBirth in year_of_birth]}")
print()
print()
"""
2. Calculate cubes of all odd numbers between 1 and 20
"""
print("2. Calculate cubes of all odd numbers between 1 and 20")
# Print the exercise's requested result
print(f"Result:{[numbers ** 3 for numbers in range(1, 21)]}")
