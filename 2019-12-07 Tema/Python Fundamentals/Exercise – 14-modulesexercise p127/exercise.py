import message
import discover

if __name__ == "__main__":
    decoded_message = discover.decode(message.secret, message.key)
    print(decoded_message)