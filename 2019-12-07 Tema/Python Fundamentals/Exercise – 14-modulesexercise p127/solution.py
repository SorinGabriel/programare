import discover
import message


if __name__ == "__main__":
    decoded_message = discover.decode(message.secret, message.key)
    # It should say "Congratulations! You've learned how modules work!"
    print(decoded_message)
