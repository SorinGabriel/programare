# We import only algorithms_guaranteed from hashlib.
from hashlib import algorithms_guaranteed
import hashlib

# We use 'string.printable' only, but * imports everything.
from string import *

# Importing by name or glob(*) adds imported names to current namespace,
# so we should use algorithms_guaranteed instead of
# hashlib.algorithms_guaranteed.

if "sha256" not in algorithms_guaranteed:
    raise Exception(
        "Expected SHA256 to be available.",
        f" Available algorithms: {algorithms_guaranteed}",
    )


def decode(secret, key):
    """Decode implements a modified version of Vigenère cipher to
    decrypt the given secret using given key. The modification is
    easy to spot: the key is first hashed using SHA256, giving us
    a 64-byte key to work with. The rest of implementation is
    standard Vigenère - each character is "rotated" backward
    using a character on corresponding possition in the hashed
    key. The rotation itself is a mechanism known from such
    ciphers as ROT13 or Caesar."""
    hashed_key = hashlib.new("sha256", key.encode()).hexdigest()
    decoded_msg = ""
    for i, ch in enumerate(secret):
        decoded_msg += _rotate_backward(ch, hashed_key[i % len(hashed_key)])
    return decoded_msg


def _encode(plaintext, key):
    """Encode has been used to create this exercise's secret
    message. It is the exact reverse of the decode function, in
    that it "rotates" forward where decode rotates backward."""
    hashed_key = hashlib.new("sha256", key.encode()).hexdigest()
    encoded_msg = ""
    for i, ch in enumerate(plaintext):
        encoded_msg += _rotate_forward(ch, hashed_key[i % len(hashed_key)])
    return encoded_msg


def _rotate_forward(character, rotation_character):
    character_index = printable.index(character)
    rotation = printable.index(rotation_character)
    return printable[(character_index + rotation) % len(printable)]


def _rotate_backward(character, rotation_character):
    character_index = printable.index(character)
    rotation = printable.index(rotation_character)
    if character_index - rotation >= 0:
        return printable[character_index - rotation]
    return printable[len(printable) - rotation + character_index]
