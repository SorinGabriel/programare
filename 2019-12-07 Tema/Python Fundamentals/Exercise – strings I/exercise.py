"""
1. Use count() to count occurrences of one or more characters in the string.
"""
print("1. Use count() to count occurrences of one or more characters in the string.")
print(f"Characters occurrences of 'a' in 'adasdasd' determined by count(): {'adasdasd'.count('a')}")
print(f"Characters occurrences of 'sd' in 'adasdasd' determined by count(): {'adasdasd'.count('sd')}")
print()
"""
2. Use find() to find the index of first occurrence of letter "a" in "anagram". Then try it again, with
"""
print("2. Use find() to find the index of first occurrence of letter \"a\" in \"anagram\". Then try it again, with")
print(f"Get the index of first occurence of letter \"a\" in \"anagram\" using find(): {'anagram'.find('a')}")
print()
"""
3. rfind() works just like find, but searches from the end of the string back to its
"""
print("3. rfind() works just like find, but searches from the end of the string back to its")
print(f"Get the index of the last occurence of letter \"a\" in \"anagram\" using rfind(): {'anagram'.rfind('a')}")
print()
"""
4. index() is like find(), but raises ValueError if the substring is not found. rindex() also exists.
"""
print("4. index() is like find(), but raises ValueError if the substring is not found. rindex() also exists.")
# Commented the line below as it throws ValueError and stops the script execution
# print(f"Get the index of first occurence of letter \"x\" in \"anagram\" using index(): {'anagram'.index('x')}")
# Commented the line below as it throws ValueError and stops the script execution
# print(f"Get the index of the last occurence of letter \"x\" in \"anagram\" using rindex(): {'anagram'.rindex('x')}")
print()
"""
5. format() has been covered in chapter 6. Printing
"""
print("5. format() has been covered in chapter 6. Printing")
print(f"Sample format, right align Test and fill empty places with dots: {'{:.>30}'.format('Test')}")
print(f"Sample format, place strings on {{0}} and {{1}}: {'This {0} is a {1}'.format('is', 'Test')}")
print()
"""
6. format_map() uses dictionary to apply the same formatting as format(). Format string "I like {something}" using format_map()
"""
print("6. format_map() uses dictionary to apply the same formatting as format(). Format string \"I like {something}\") using format_map()")
print(f"Formatting string \"I like {{something}}\") using format_map(): {'I like {something}'.format_map({'something' : 'pizza'})}")
print()
"""

7. Use startswith() to check whether "pre" is a prefix of "preamble"
"""
print("7. Use startswith() to check whether \"pre\" is a prefix of \"preamble\"")
print(f"Checking if 'pre' is a prefix of 'preamble' using startswith() string function: {'preamble'.startswith('pre')}")
print()
"""
8. Use endswith() to check, whether "tle" is the suffix of "my castle"
"""
print("8. Use endswith() to check, whether \"tle\" is the suffix of \"my castle\"")
print(f"Checking if 'tle' is a suffix of 'my castle' using endswith() string function: {'my castle'.endswith('tle')}")
print()
"""
9. Use strip(<iterable>) to remove all occurences of any of the symbols in iterable from string's beginning and end.
"""
print("9. Use strip(<iterable>) to remove all occurences of any of the symbols in iterable from string's beginning and end.")
print(f"Stripping dots from '....adasdasd.....' string using strip(): {'....adasdasd.....'.strip('.')}")
print(f"Stripping white spaces from '       adasdasd       ' string using strip(): {'       adasdasd       '.strip()}")
print()
"""
10. Likewise, you can use either lstrip() or rstrip() to remove symbols from string's beginning and end respectively.
"""
print("10. Likewise, you can use either lstrip() or rstrip() to remove symbols from string's beginning and end respectively.")
print(f"Stripping dots from the beginning of '....adasdasd.....' string using lstrip(): {'....adasdasd.....'.lstrip('.')}")
print(f"Stripping white spaces from the beginning of '       adasdasd       ' string using lstrip(): {'       adasdasd       '.lstrip()}")
print(f"Stripping dots from the end of '....adasdasd.....' string using rstrip(): {'....adasdasd.....'.rstrip('.')}")
print(f"Stripping white spaces from the end of '       adasdasd       ' string using rstrip(): {'       adasdasd       '.rstrip()}")
print()
"""
11. Use split(<separator>) to split "first, second, third" into a list.
"""
print("11. Use split(<separator>) to split \"first, second, third\" into a list.")
print(f"Splitting \"first, second, third\" into a list using split() function: {'first, second, third'.split(', ')}")
print()
"""
12. Use splitlines() to split "first\nsecond" into a list.
"""
print("12. Use splitlines() to split \"first\nsecond\" into a list.")
print(f"Split \"first\nsecond\" into a list using splitlines() function:")
print('first\nsecond'.splitlines())
print()
"""
13. Use partition() to split string "Address: Rosewood St. 2113", assuming ":" is the separator. What is the result?
"""
print("13. Use partition() to split string \"Address: Rosewood St. 2113\", assuming\":\" is the separator. What is the result?")
print(f"Splitting string \"Address: Rosewood St. 2113\", with \":\" separator using partition(): {'Address: Rosewood St. 2113'.partition(':')}")
print()
"""
14. Use ", ".join(<list of strings>) with range(5) to get a string with comma-separated numbers. Use comprehension to convert numbers to strings first.
"""
print("14. Use ", ".join(<list of strings>) with range(5) to get a string with comma-separated numbers. Use comprehension to convert numbers to strings first.")
print(f"Merging comma separated numbers into a string using join() function: {', '.join([str(number) for number in range(5)])}")
print()
"""
15. Use replace() to replace all occurences of "X" to your name in "Welcome, X. Is it OK if I call you X?"
"""
print("15. Use replace() to replace all occurences of \"X\" to your name in \"Welcome, X. Is it OK if I call you X?\"")
print(f"Replacing all occurences of \"X\" to your name in \"Welcome, X. Is it OK if I call you X?\"  using replace() function: {'Welcome, X. Is it OK if I call you X?'.replace('X', 'Sorin')}")
print()
"""
16. Use center() with arbitrary length to center any string.
"""
print("16. Use center() with arbitrary length to center any string.")
print(f"Testing center() function: {'adasdasdasd'.center(100)}")
print()
"""
17. Using ljust() and rjust() achieve the same result as "x".center(11)
"""
print("17. Using ljust() and rjust() achieve the same result as \"x\".center(11)")
print(f"\"x\".center(11):")
print(f"{'x'.center(11)}")
print(f"Now achieving the same result as above using ljust() and rjust():")
print(f"{'x'.rjust(6).ljust(11)}")
print()
"""
18. Check whether all characters in string "19²" are digits using isdigit().
"""
print("18. Check whether all characters in string \"19²\" are digits using isdigit().")
print([(letter + " is digit: " + str(letter.isdigit())) for letter in '19²'])
print()
"""
19. Check whether all characters in string "imminent" are alphabetic using isalpha().
"""
print("19. Check whether all characters in string \"imminent\" are alphabetic using isalpha().")
print([(letter + " is alphabetic: " + str(letter.isalpha())) for letter in 'imminent'])
print()
"""
20. Use isalnum() to check, whether string \"Rose Hightower, Sailsbury Rd 92\" contains alphanumerics only. Fix it if it contains anything else.
"""
print("20. Use isalnum() to check, whether string \"Rose Hightower, Sailsbury Rd 92\" contains alphanumerics only. Fix it if it contains anything else.")
print(f"\"Rose Hightower, Sailsbury Rd 92\" contains alphanumerics only: {'Rose Hightower, Sailsbury Rd 92'.isalnum()}")
print(f"Result after clearing anything non-alphanumeric from it:")
fixedString = ''.join([letter for letter in 'Rose Hightower, Sailsbury Rd 92' if letter.isalnum()])
print(fixedString +f" is alphanumeric: {fixedString.isalnum()}")
print()
"""
21. Use isspace()to check if string \" \t\n\" contains whitespace only.
"""
print("21. Use isspace() to check if string \" \t\n\" contains whitespace only.")
print(" \t\n".isspace())

print()
"""
22. Use lower() to convert "mIxED caSe sTRING" to lowercase.
"""
print("22. Use lower() to convert \"mIxED caSe sTRING\" to lowercase.")
print(f"{'mIxED caSe sTRING'.lower()}")
print()
"""
23. Use islower() to check if all letters in your string of choice are lowercase.
"""
print("23. Use islower() to check if all letters in your string of choice are lowercase.")
print(f"String 'testIng' has only lowercase characters: {'testIng'.islower()}")
print(f"String 'testing' has only lowercase characters: {'testing'.islower()}")
print(f"String ' ' has only lowercase characters: {' '.islower()}")
print(f"String '' has only lowercase characters: {''.islower()}")
print()
"""
24. Use title() to capitalize every word in "this could be a title"
"""
print("24. Use title() to capitalize every word in \"this could be a title\"")
print(f"{'this could be a title'.title()}")
print()
"""
25. Use capitalize() to capitalize the following string "morocco is a beautiful country."
"""
print("25. Use capitalize() to capitalize the following string \"morocco is a beautiful country.\"")
print(f"{'morocco is a beautiful country.'.capitalize()}")
print()
"""
26. Use istitle() to check whether the string "illiad" is titlecased. Use capitalize() to fix it and then check again.
"""
print("26. Use istitle() to check whether the string \"illiad\" is titlecased. Use capitalize() to fix it and then check again.")
print(f"'illiad' is titlecased: {'illiad'.istitle()}")
fixedString = 'illiad'.title()
print(fixedString + f' is now titlecased: {fixedString.istitle()}')
print()
"""
27. Use upper() to convert a string of your choice to upper-case.
"""
print("27. Use upper() to convert a string of your choice to upper-case.")
print(f"Converting 'adasdasd' to upper-case: {'adasdasd'.upper()}")
print()
"""
28. Check whether a string of your choice is uppercased using isupper().
"""
print("28. Check whether a string of your choice is uppercased using isupper().")
print(f"String 'testIng' has only uppercased characters: {'testIng'.isupper()}")
print(f"String 'testing' has only uppercased characters: {'testing'.isupper()}")
print(f"String 'TESTING' has only uppercased characters: {'TESTING'.isupper()}")
print(f"String ' ' has only uppercased characters: {' '.isupper()}")
print(f"String '' has only uppercased characters: {''.isupper()}")
print()
"""
29. Use swapcase() on "A vErY mIxEd Case string". What does it do?
"""
print("29. Use swapcase() on \"A vErY mIxEd Case string\". What does it do?")
print(f"{'A vErY mIxEd Case string'.swapcase()}")
print()
"""
30. Use casefold() on "Straße" (German for street) and then compare it to "strasse"
"""
print("30. Use casefold() on \"Straße\" (German for street) and then compare it to \"strasse\"")
caseFoldedString = "Straße".casefold()
print(f"Straße after calling casefold() on it: {caseFoldedString}")
print(f"Case folded string is equal to 'strasse': {caseFoldedString == 'strasse'}")
print(f"")
print()
"""
31. Use encode() to encode UTF-8 string "garçon"
"""
print("31. Use encode() to encode UTF-8 string \"garçon\"")
print(f"{'garçon'.encode('UTF-8')}")
print()