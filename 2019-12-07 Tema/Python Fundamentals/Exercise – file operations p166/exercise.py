"""
Use either open/close, or with … open():
"""
"""
1. Exercise 1
• Read example file data/exercise.txt line-by-line.
• Knowing that a line’s format is title (string), director (string), year (integer), parse all of them
• Print a list of dictionaries.
• Remember to strip linebreak sign.
"""
print("1. Exercise 1")

print("• Read example file data/exercise.txt line-by-line.")
print("data/exercise.txt file content: ")
with open('data/exercise.txt') as f:
    for line in f:
        clean_line = line.rstrip()
        # skip empty lines
        if clean_line == "":
            continue
        print(clean_line)
print()

print("• Knowing that a line’s format is title (string), director (string), year (integer), parse all of them")
with open('data/exercise.txt') as f:
    for index, line in enumerate(f):
        clean_line = line.rstrip()
        # skip empty lines
        if clean_line == "":
            continue
        (title, director, year) = [lineContent.strip() for lineContent in clean_line.split(', ')]
    print(f"Row {index + 1}")
    print(f"Title is: {title}")
    print(f"Director is: {director}")
    print(f"Year is: {year}")
print()

print("• Print a list of dictionaries.")
print("• Remember to strip linebreak sign.")
fileContent = []
with open('data/exercise.txt') as f:
    for index, line in enumerate(f):
        clean_line = line.rstrip()
        # skip empty lines
        if clean_line == "":
            continue
        (title, director, year) = [lineContent.strip() for lineContent in clean_line.split(', ')]
        fileContent.append({"title": title, "director": director, "year": year})

print(fileContent)
print()

"""
2. Exerise 2
• Open example file data/secret-message.txt in binary mode
• Read bytes 517-560 (43 bytes)
• Decode them to string
• Split it using + as separator
• Save resulting list by writing it to a new file. Use either write() in a loop or writelines().
"""
print("2. Exerise 2")
print("• Open example file data/secret-message.txt in binary mode")
print("• Read bytes 517-560 (43 bytes)")
with open('data/secret-message.txt', 'rb') as file:
    index = 517
    file.seek(index)
    targetBytes = bytearray(file.read(43))
    print(targetBytes)
stringDecoded = targetBytes.decode('utf-8')
print()

print("• Decode them to string")
print(stringDecoded)
print()

print("• Split it using + as separator")
strings = stringDecoded.split("+")
print(strings)
print()

print("• Save resulting list by writing it to a new file. Use either write() in a loop or writelines().")
newFile = open('data/decoded-secret-message.txt', 'w')
newFile.writelines([string + "\n" for string in strings])
newFile.close()

newFile = open('data/decoded-secret-message.txt', 'r')
print(newFile.read())
