"""
1. Define a new class User with two fields: name and _password
2. Make sure password is a private field using a single _
3. name has to be supplied during initialization
4. Try printing u._password outside of class scope. Does it work?
5. Add set_password() method, that allows user to change password. The password has to be at least 6
characters long. If it is shorter, it is filled with = up to 6 characters.
6. Add get_password() method, which allows user to display password with only first and last letter
visible, the rest substituted with * symbols.
7. Change _password definition, so that its name is mangled with class name: __password
8. Try printing u.__password outside of class scope. Does it work?
"""


class User:
    name = ''
    __password = "secret"

    def __init__(self, name):
        self.name = name

    def set_password(self, password):
        self.__password = '{:=<6}'.format(password)

    def get_password(self):
        passwordLength = len(self.__password)
        return ''.join(['*' if (0 < index < passwordLength - 1) else letter for index, letter in enumerate(self.__password)])

user1 = User("Andrei")
# Request 4. Looks like it works, only that IDE warns this behavior
# Request 7 & 7. Printing __attribute outside of class does not work, throws error
#print(user1.__password)
print(user1.get_password())
user1.set_password("")
#print(user1.__password)
print(user1.get_password())
user1.set_password("ABCDE")
#print(user1.__password)
print(user1.get_password())
user1.set_password("ABCDEFGHI")
#print(user1.__password)
print(user1.get_password())
