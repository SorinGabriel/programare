# Generate a formatted full name
def formatted_name(first_name, last_name, middle_name=''):
    return ' '.join([name.strip() for name in [first_name, middle_name, last_name] if len(name) > 0]).title()

# Convert a string of "STR1!!!STR2!!!...STRn-1!!!STRn" structure into "@STRn#STRn-1#...STR2#STR1#" format
def formatted_name_florin2(florin):
    # Gather the list of strings by separating the given string by !!!
    namelist = [name + "#" for name in florin.split('!!!') if len(name) > 0]
    # Reverse the obtained list
    namelist.reverse()
    # Eliminate any digits from the resulting strings
    return '@' + ''.join([''.join([letter for letter in name if not letter.isdigit()]) for name in namelist])
