import unittest
from name_function import formatted_name, formatted_name_florin2


class NamesTestCase(unittest.TestCase):

    def test_first_last_name(self):
        result = formatted_name("pete", "seeger")
        self.assertEqual(result, "Pete Seeger")

    def test_first_last_middle_name(self):
        result = formatted_name("raymond", "reddington", "red")
        self.assertEqual(result, "Raymond Red Reddington")

    def test_formatted_name_florin2(self):
        result = formatted_name_florin2("DITA!!!FLORIN")
        self.assertEqual(result, "@FLORIN#DITA#")

    def test_formatted_name_florin22(self):
        result = formatted_name_florin2("DI15TA15!!!F4LORI5N")
        self.assertEqual(result, "@FLORIN#DITA#")
