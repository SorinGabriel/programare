def calcul_math(opt_type, x, opt10, msg="Salut"):
    if not isinstance(opt_type, int):
        return
    if not isinstance(x, int):
        return
    if not isinstance(opt10, int):
        return

    if opt_type == 1:
        result = x * opt10
    elif opt_type == 2:
        result = x % opt10
    elif opt_type == 3:
        result = x ** (1 / opt10)
    else:
        return

    if msg != "Salut":
        result = msg + str(result)
    else:
        result = str(result)
    return result
