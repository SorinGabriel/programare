import unittest
from main_prog import calcul_math


class NamesTestCase(unittest.TestCase):

    def test_1(self):
        result = calcul_math(1, 2, 3)
        self.assertEqual(result, "6")

    def test_2(self):
        result = calcul_math(1, 2, 3, "Demo Message ")
        self.assertEqual(result, "Demo Message " + str(2 * 3))

    def test_3(self):
        result = calcul_math(2, 4, 3)
        self.assertEqual(result, str(4 % 3))

    def test_4(self):
        result = calcul_math(3, 27, 3)
        self.assertEqual(result, str(27 ** (1 / 3)))

    def test_5(self):
        result = calcul_math(10, 27, 3)
        self.assertEqual(result, None)

    def test_6(self):
        result = calcul_math(1.8,1, 2)
        self.assertEqual(result, None)

    def test_7(self):
        result = calcul_math(1,1.9, 2)
        self.assertEqual(result, None)