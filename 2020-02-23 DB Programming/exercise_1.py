import pymysql
import mysql.connector
import csv
from datetime import datetime

db = mysql.connector.connect(user='root', password='123456',
                              host='127.0.0.1',
                              database='default')
c = db.cursor()
c.execute("""
    CREATE TABLE IF NOT EXISTS `default`.`bikesharing` (
          `tstamp` TIMESTAMP NULL,
          `cnt` INT NULL,
          `temperature` DECIMAL(5,2) NULL,
          `temperature_feels` DECIMAL(5,2) NULL,
          `humidity` DECIMAL(4,1) NULL,
          `wind_speed` DECIMAL(5,2) NULL,
          `weather_code` INT NULL,
          `is_holiday` TINYINT NULL,
          `is_weekend` TINYINT NULL,
          `season` INT NULL
    );
""")
c.execute("TRUNCATE TABLE bikesharing")
with open("london-bikes.csv") as f:
    reader = csv.DictReader(f)
    count = 1
    for row in reader:
        row['tstamp'] = '"' + row.pop('timestamp', None) + '"'
        c.execute(f"INSERT INTO bikesharing (`{'`,`'.join(list(row.keys()))}`) VALUES ({','.join(list(row.values()))})")
        if count % 100 == 0:
            db.commit()
        count += 1

db.close()
