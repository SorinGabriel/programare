import pymysql
import mysql.connector
import csv
from datetime import datetime

db = mysql.connector.connect(user='root', password='123456',
                              host='127.0.0.1',
                              database='default')
c = db.cursor()
"""
● fetch total sum of new shares by season
"""
c.execute("""
    SELECT 
        SUM(cnt) AS sum, season
    FROM
        bikesharing
    GROUP BY season
""")
print(c.fetchall())
"""
● fetch total sum of new shares during thunderstorms
"""
c.execute("""
    SELECT 
        SUM(cnt) AS sum
    FROM
        bikesharing
    WHERE weather_code = 10
""")
print(c.fetchall())
"""
● fetch the date and hour with the most new shares
"""
c.execute("""
    SELECT 
        DATE(tstamp), HOUR(tstamp), cnt
    FROM
        bikesharing
    ORDER BY cnt DESC
    LIMIT 100
""")
"""
● Add 10 to cnt column for all 2015-01-09 entries
"""
c.execute("""
    UPDATE bikesharing 
    SET 
        cnt = cnt + 10
    WHERE
        DATE(tstamp) = '2015-01-09'
""")

db.close()
