r"""
Given tuple `recipe = ("boil water", "insert egg", "wait 5min", "eat")`
>>> recipe = ("boil water", "insert egg", "wait 5min", "eat")

1. Use getitem operator `[]` to get 3rd step of the recipe. Remember that indices start at 0!
>>> recipe[2]
'wait 5min'

2. Print a slice of the last two steps of the recipe.
>>> recipe[len(recipe)-2:]
('wait 5min', 'eat')

3. Count occurrences of "wait 5min" using `count()` function.
>>> recipe.count("wait 5min")
1

4. Check whether `"boil water"` is the first step using `index()` function.
>>> recipe.index("boil water")
0

"""
