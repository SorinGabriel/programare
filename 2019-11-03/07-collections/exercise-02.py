"""
Given a list `["User1", "UserChris", "User2", "Admin"]`:
>>> users = ["User1", "UserChris", "User2", "Admin"]

1. Print a slice containing only `"User2"`
>>> result1 = users[2:3]
>>> result1
['User2']

2. Print a slice of all users, except the first one
>>> users[1:]
['UserChris', 'User2', 'Admin']

3. Print a slice of all users, except the `"Admin"`
>>> users[:len(users)-1]
['User1', 'UserChris', 'User2']

4. Print a slice of the first two users
>>> result4 = users[:2]
>>> result4
['User1', 'UserChris']

"""
