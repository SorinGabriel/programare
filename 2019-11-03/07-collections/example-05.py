unique_colours = set()  # this is an empty set
print(f"Current length of 'unique': {len(unique_colours)}")

# Let's add some elements
unique_colours.add("red")
unique_colours.add("yellow")
unique_colours.add("blue")
print(f"Unique colours: {unique_colours} (length: {len(unique_colours)})")

# Let's try to add a non-unique value.
unique_colours.add("yellow")
print(f"Unique colours: {unique_colours} (length: {len(unique_colours)})")
