L = [1, 2, 3, 4, 5]
#1
print("Length of the list L is:", len(L))
#2
L.append(2)
print("The modified list is: ", L)
#3
print("The number of 2's from the list: ", L.count(2))
#4
print("The number of 7's which are not in the list:", L.count(7))
#5
L.extend([6, 7, 8])
print("The modified list is: ", L)
#6
print("The first occurence of 7 in the list is at the index: ", L.index(7))
#7
L.insert(0, 10)
print("The modified list is: ", L)
#8
print("The value of the last element from the list is:", L[-1])
#9
print("The value of the last element from the list is:", L.pop())
print("The modified list is: ", L)
#10
L.remove(4)
print("The modified list after removing 4 is:", L)
#11
L.reverse()
print("The reversed list is:", L)
#12
L.sort()
print("The sorted list is:", L)
#13
L.clear()
print("The list after calling clear() is:", L)