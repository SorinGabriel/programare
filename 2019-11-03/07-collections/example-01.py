alphabet = []  # this is an empty list
print(f"Current length of 'alphabet': {len(alphabet)}")

# Let's add some letters
alphabet.append("a")
alphabet.append("b")
alphabet.append("c")
print(f"Alphabet: {alphabet} (length: {len(alphabet)})")

# Indexing
print(f"The first letter of alphabet is '{alphabet[0]}'")

# Indexing is cyclical
print(
    f"""
-3: {alphabet[-3]}
-2: {alphabet[-2]}
-1: {alphabet[-1]}
 0: {alphabet[0]}
 1: {alphabet[1]}
 2: {alphabet[2]}"""
)

# Let's add some more letters
alphabet.extend(["f", "d", "g", "e"])
print(f"Alphabet (mixed): {alphabet} (length: {len(alphabet)})")

alphabet.sort()
print(f"Alphabet (sorted): {alphabet} (length: {len(alphabet)})")
