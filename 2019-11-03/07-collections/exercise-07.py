r"""
1. Try zipping a dictionary, a tuple, and a list together.
>>> capitals = {"Peru": "Lima", "USA": "Washington DC", "Norway": "Oslo"}
>>> languages = ("spanish", "english", "norwegian")
>>> population_M = [33.0, 326.0, 5.3]
>>> list(zip(capitals, languages, population_M))
[('Peru', 'spanish', 33.0), ('USA', 'english', 326.0), ('Norway', 'norwegian', 5.3)]

2. Try zipping iterables with different lengths. What happens?
>>> longer = ["cat", "dog", "horse"]
>>> shorter = ["milk", "bone"]
>>> dict(zip(longer, shorter))
{'cat': 'milk', 'dog': 'bone'}

3. Try zipping only one iterable.
>>> list(zip(["a", "b"]))
[('a',), ('b',)]

4. Try zipping nothing.
>>> z = zip()
>>> type(z)
<class 'zip'>
>>> list(z)
[]

"""
