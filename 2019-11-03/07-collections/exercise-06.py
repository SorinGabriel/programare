r"""
Try guessing output of each line.
>>> {"A": 1, "B": 2} == {"A": 1}
False

>>> [0, 1, 2] == [0, 1, 2]
True

>>> 1 in (1, 2, 3)
True

>>> 1 not in {"A": 1}
True

>>> 1 in {"A": 1}.values()
True

>>> [5, 4] == (5, 4)
False

"""
