r"""
Given list `[1, 2, 3, 4, 5]`
>>> l = [1, 2, 3, 4, 5]

1. Use built-in `len()` function to print its length
>>> len(l)
5

2. Using `append()` list function, append a sixth element: another `2`
>>> l.append(2)
>>> l
[1, 2, 3, 4, 5, 2]

3. Use `count()` list function to count the `2`'s in list
>>> l.count(2)
2

4. Use the same function to count `7`'s, which are not in the list. What’s the result?
>>> l.count(7)
0

5. Use `extend()` list function to extend it with the following `[6, 7, 8]`
>>> l.extend([6, 7, 8])
>>> l
[1, 2, 3, 4, 5, 2, 6, 7, 8]

6. Use `index()` list function to check index of the `7`. Use indexing to check you got the correct result.
>>> l.index(7)
7
>>> l[7]
7

7. Use `insert()` to add a value `10` at index `0`. Print the list to check what it did.
>>> l.insert(0, 10)
>>> l
[10, 1, 2, 3, 4, 5, 2, 6, 7, 8]

8. Use `[-1]` indexing to check the value of the last element.
>>> l[-1]
8

9. Use `pop()` to check the value of the last element, removing it from the list at the same time.
>>> l.pop()
8

10. Use `remove()` to delete `4` from the list
>>> l.remove(4)
>>> l
[10, 1, 2, 3, 5, 2, 6, 7]

11. Use `reverse()` and print the list.
>>> l.reverse()
>>> l
[7, 6, 2, 5, 3, 2, 1, 10]

12. Use `sort()` and print it again.
>>> l.sort()
>>> l
[1, 2, 2, 3, 5, 6, 7, 10]

13. Use clear to empty the list
>>> l.clear()
>>> l
[]

"""
