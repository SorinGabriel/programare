r"""Unformatted output - taking advantage of print function's definition.
>>> help(print)
Help on built-in function print in module builtins:

print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)

    Prints the values to a stream, or to sys.stdout by default.
    Optional keyword arguments:
    file:  a file-like object (stream); defaults to the current sys.stdout.
    sep:   string inserted between values, default a space.
    end:   string appended after the last value, default a newline.
    flush: whether to forcibly flush the stream.
"""
import sys


print("What", "a", "lovely", "day.")

print("1", "2", 3, 4, 5)

fruit = "oranges"
print("apples", "bananas", fruit, sep=", ")

print("Printing in progress... ", end="")
print("DONE")

print("This kind of output is often used by loggers.", file=sys.stderr, flush=True)

val = 400

