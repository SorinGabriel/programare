var = 100
print(
    "Inventarul este: {0} sapa, {0} grebla, {0} cazmala, {0} ceva, {0} lopata, {0} sticla, {0} altceva".format(
        var
    )
)

var = 250
print(
    "Inventarul este: {0} sapa, {0} grebla, {0} cazmala, {0} ceva, {0} lopata, {0} sticla, {0} altceva".format(
        var
    )
)

var1 = 400
var2 = 500
# Parsare prin valoare
print(
    "Inventarul este: {1} sapa, {0} grebla, {1} cazmala, {0} ceva, {1} lopata, {0} sticla, {1} altceva".format(
        var1, var2
    )
)

var1 = 400
var2 = 500
# Parsare prin referinta
print(
    "Inventarul este: {var1} sapa, {var2} grebla, {var1} cazmala, {var2} ceva, {var1} lopata, {var2} sticla, {var1} altceva".format(
        var1=var1,
        var2=var2
    )
)

var1 = "Ana"
var2 = "Bob"
print(
    "{var1} met {var2}".format(
        var1=var1,
        var2=var2
    )
)


#1
print()

print(
    "| " + f"{'User':^20}" +  " | " + f"{'Days Active':^20}" +  " | " + f"{'Time Spent On Site (h)':^30}" +  " |\n" +
     f"{'':->80}\n" +
    "| " + f"{'Katie':<20}" + " | " + f"{'10':<20}" +           " | " + f"{'27.30':<30}" +                  " |\n" +
    "| " + f"{'Ben':<20}" +   " | " + f"{'21':<20}" +           " | " + f"{'89.12':<30}" +                  " |\n" +
    "| " + f"{'Adam':<20}" +  " | " + f"{'1':<20}" +            " | " + f"{'0.50':<30}" +                   " |\n" +
    "| " + f"{'Jodie':<20}" + " | " + f"{'2':<20}" +            " | " + f"{'6.00':<30}" +                   " |"
)

#2
print()

n = 257
print(
    "Binary format:",
    f"{n:b}"+";",
    "Octal format:",
    f"{n:o}"+";",
    "Hexadecimal format:",
    f"{n:x}"+";"
)

#3
print()

import math
print(
    "PI with 27 decimals:",
    f"{math.pi:.27f}"
)




