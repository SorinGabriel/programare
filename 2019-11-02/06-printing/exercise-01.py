r"""
1. What does :20 do in f"{'left':20}"?
>>> f"{'left':20}"
'left                '

2. Is it the same as calling f"{'left':<20}"?
>>> f"{'left':<20}"
'left                '

3. Try substituting `<` with `<` and `^`.
>>> f"{'left':>20}"
'                left'
>>> f"{'left':^20}"
'        left        '

4. Try to guess what output do you get from each print. Can you tell why?
>>> page_number = 10
>>> f"Chapter 1 {page_number:.>30}"
'Chapter 1 ............................10'

>>> n = 109.432188881111
>>> f"{n}"
'109.432188881111'
>>> f"{n:f}"
'109.432189'
>>> f"{n:.12f}"
'109.432188881111'
>>> f"{n:.1f}"
'109.4'

>>> voters_percentage = 0.71
>>> f"{voters_percentage:.1%}"
'71.0%'

>>> to_binary = 1024
>>> f"{to_binary:b}"
'10000000000'

"""
