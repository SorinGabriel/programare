import math

equation = 10 ** ((16 ^ 2) % 3)
equation2 = 16 ^ 2
print(f"Result of the equation: {equation}")
print(f"Result of the equation: {equation2}")

author = "Aldous Huxley's"
title = "Brave New World"
published = 1932
# Hint: to print a double quotation mark you can either use single quotes
# to delimit the string, or escape it using a backslash. Both methods
# are presented below.
print(f'{author} "{title}", published in {published}, is a dystopian novel.')
print(f'{author} "{title}", published in {published}, is a dystopian novel.')
# Hint: you can also escape curly brackets by using two brackets.
print(f"This is a curly bracket symbol: {{")
print(f"The value of Pi is approximately {math.pi:.48f}")
print(f"The value of Pi is approximately {math.pi:.50f}")
print(f"The value of Pi is approximately {math.pi:.60f}")

header1 = "Name"
header2 = "Age"
name = "Beth"
age = 63
header_row = f"| {header1:10} | {header2:10} |"
print(header_row)
print("-" * len(header_row))
print(f"| {name:10} | {age:10} |")

print(f"{'left':^25}")

page_number = 10
print(f"Chapter 1 {page_number:.>30}")
n = 109.472188881111
print(f"{n}")
print(f"{n:f}")
print(f"{n:.10f}")
print(f"{n:.1f}")

voters_percentage = 0.7127
print(f"{voters_percentage:.1%}")

voters_percentage = 0.7127
print(f"{voters_percentage:.2%}")

to_binary = 1024
print(f"{to_binary:b}")

to_binary = 256
print(f"{to_binary:b}")
to_binary = 512
print(f"{to_binary:b}")
to_binary = 1
print(f"{to_binary:b}")

to_binary = 2
print(f"{to_binary:b}")

to_binary = 3
print(f"{to_binary:b}")
