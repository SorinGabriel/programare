print("integers: %d, %d, %d" % (1, 2, 42))
print("octal: %o, %o, %o" % (1, 2, 42))
print("hexadecimal: %x, %x, %x" % (1, 2, 42))

print("floating point: %f" % 3.1415)
print("floating point (to the 2nd place): %.2f" % 3.1415)
print("exponential: %e" % 14 ** 8)

print("string: %s" % "a string!")
print("repr: %r" % "a string!")

print("character: %c" % "A")
mysterious_numbers = (104, 101, 108, 108, 111, 33)
print("characters: %c%c%c%c%c%c" % mysterious_numbers)

print("percent: %d%%" % 71)  # '%%' produces a '%' sign
