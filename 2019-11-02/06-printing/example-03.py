print("{0} met {1}".format("Ann", "Bob"))
print("{1} met {0}".format("Ann", "Bob"))

print(
    "User {user_name!r} completed {action}.".format(user_name="user1", action="sign in")
)

print(
    "{0}, {1} and {how_many} others liked your post".format("Ann", "Bob", how_many=216)
)


# We use a list in this example. Don't worry we will cover lists in
# Chapter 13 "Collections".
people_who_liked_your_photo = ["Ann", "Bob", "Matt", "Henry", "Julie", "Katie"]
print(
    "{people[0]} and {people[1]} liked your photo".format(
        people=people_who_liked_your_photo
    )
)
