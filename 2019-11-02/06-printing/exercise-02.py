r"""
1. Print the following table. Focus on matching text alignment.
|       User      |    Days active    |      Time spent on site (h)      |
--------------------------------------------------------------------------
| Katie           | 10                | 27.30                            |
| Ben             | 21                | 89.12                            |
| Adam            | 1                 | 0.50                             |
| Jodie           | 2                 | 6.00                             |
>>> names = ["Katie", "Ben", "Adam", "Jodie"]
>>> days = [10, 21, 1, 2]
>>> time = [27.3, 89.12, 0.5, 6.0]
>>> print(f"| {'User':^25} | {'Days active':^25} | {'Time spent on site (h)':^40} |")
|           User            |        Days active        |          Time spent on site (h)          |
>>> print("-" * 100)
----------------------------------------------------------------------------------------------------
>>> for i in range(len(names)):
...     print(f"| {names[i]:<25s} | {days[i]:<25d} | {time[i]:<40.2f} |")
| Katie                     | 10                        | 27.30                                    |
| Ben                       | 21                        | 89.12                                    |
| Adam                      | 1                         | 0.50                                     |
| Jodie                     | 2                         | 6.00                                     |



2. What is 257 in binary, octal and hexadecimal representations? Print those values 
out in one line using an f-string and knowing, that `:b` represents binary, 
`:o` octal and `:x` hexadecimal.
>>> num = 257
>>> print(f"binary={num:b}, octal={num:o}, hexadecimal={num:x}")
binary=100000001, octal=401, hexadecimal=101

3. What is the 27th digit of Pi after the decimal point? Find out by printing `math.pi` 
to the 27th digit. Don’t forget to import mathematics library by using `import math`!
>>> import math
>>> print(f"{math.pi:.27f}")
3.141592653589793115997963469

"""
