# Arithmetic
# Arithmetic operators (str)
print("Arithmetic operators")
# 4 (int)
print(1 + 2 + 5 - (2 * 2))
# 401.0001 (float)
print(501.0 - 99.9999)
# 8 (int)
print(2 ** 3)
# 2.5 (float)
print(10.0 / 4.0)
# 2 (int)
print(10.0 // 4.0)
# 1 (int)
print(5 % 2)

# Comparison
# Comparison operators (str)
print("Comparison operators")
print(2 == 2)
print(1 != 1)
print(99 > 1.1)

# Logical
print("Logical operators")
print(True or False)
print(False and False and True)
print(not False)

# Membership
print("Membership operators")
print("fox" not in "cow, sheep, dog")
print("wine" in "France is famous for its wines")


