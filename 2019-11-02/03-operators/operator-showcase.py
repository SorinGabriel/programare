r"""The correct output of each line starting with `>>>` is right below it.

Arithmetic operators
>>> 5 + 5
10
>>> 30.1 - 0.71
29.39
>>> 2 * 2
4
>>> 3 ** 3
27
>>> 2.5 / 5.0
0.5
>>> 2.5 // 2.0
1.0
>>> 10 % 3
1

Comparison operators
>>> True == True
True
>>> 1 != 3
True
>>> 2 < 3
True
>>> 2 > 500
False
>>> 2 >= 2
True
>>> 2 <= 2
True

Assignment operators
>>> num = 10
>>> print(num)
10
>>> num += 1
>>> print(num)
11
>>> num -= 3
>>> print(num)
8
>>> num *= -0.5
>>> print(num)
-4.0
>>> num **= 2
>>> print(num)
16.0
>>> num /= 3
>>> print(num)
5.333333333333333
>>> num //=2
>>> print(num)
2.0
>>> num %= 2
>>> print(num)
0.0

Identity operators
>>> 5 is 5
True
>>> 1 is not 2
True

Logical operators
>>> False or False or False
False
>>> False or True
True
>>> True and True
True
>>> True and False
False
>>> not False
True
>>> not True
False
>>> True and not False
True

Membership operators
>>> "yes" in "yes, no"
True
>>> 1 in [1, 2, 3]
True
>>> 2 not in [4, 5, 6]
True

Bitwise operators
>>> int("1101", 2) & int("1111", 2) == int("1101", 2)
True
>>> int("1001", 2) | int("0110", 2) == int("1111", 2)
True
>>> int("1010", 2) ^ int("1000", 2) == int("0010", 2)
True
>>> ~int("0", 2) == int("-1", 2)
True
>>> int("0010", 2) << 1 == int("0100", 2)
True
>>> int("0010", 2) << 2 == int("1000", 2)
True
>>> int("0010", 2) >> 1 == int("0001", 2)
True
>>> int("0001", 2) >> 1 == int("0000", 2)
True

"""
