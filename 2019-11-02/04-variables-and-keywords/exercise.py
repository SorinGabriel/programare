r"""
Exercise - experiment with variables

1. Create a variable called `example` and assign `None` to it, then print its type.
>>> example = None
>>> print(type(example))
<class 'NoneType'>

2. Add two more lines:
    1. first: assigning a new value "yes" to `example`
    2. second: printing its type again
>>> example = "yes"
>>> print(type(example))
<class 'str'>

3. Create to variables containing integers. Print the result of adding them together.
>>> a = 10
>>> b = 3
>>> print(a+b)
13

4. Variable-to-variable assignment:
    1. Create a variable called `source_var` and assign "Magic" to it.
    2. Create `copy_var` and assign `source_var` to it
    3. Print the value of `copy_var`. Is it the same as `source_var`?
>>> source_var = "Magic"
>>> copy_var = source_var
>>> print(copy_var)
Magic

"""
