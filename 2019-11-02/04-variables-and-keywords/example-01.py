number = 10  # This is how you assign a value to a variable.
number = -1  # You can change variable's value like this.
# Assignment alone does not produce any output; We still need
# to use print.
print(number)
# 1
example = None
print(type(example))
# 2
example = "Yes"
print(type(example))
# 3
int1 = 10
int2 = 15
print(int1 + int2)
# 4
source_var = "Magic"
copy_var = source_var
print(copy_var)
print(copy_var == source_var)
print(copy_var is source_var)
