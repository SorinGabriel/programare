# This example is beyond scope of the "Variables and keywords" lecture.
# However it is a good practice to show where the knowledge comes from.
# Documentation: https://docs.python.org/3/library/keyword.html
import keyword

print(f"Here's a list of all {len(keyword.kwlist)} keywords:\n{keyword.kwlist}")

# Here's another way to discover them
help("keywords")
