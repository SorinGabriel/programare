# This is fine:
pangram_en = "How vexingly quick daft zebras jump!"
pangram_fr = "Voix ambiguë d'un cœur qui, au zéphyr, préfère les jattes de kiwis."
pangram_de = '"Fix, Schwyz!", quäkt Jürgen blöd vom Paß.'
hello_cn = "你好，世界。"
print(pangram_en, pangram_fr, pangram_de, hello_cn)

# This is STRONGLY discouraged (but it works):
整数 = 7
print(整数)
