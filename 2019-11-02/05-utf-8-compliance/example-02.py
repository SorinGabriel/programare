# https://en.wikipedia.org/wiki/List_of_Unicode_characters
print("\u00DCnic\u00F6de")

# https://en.wikipedia.org/wiki/Emoji#Unicode_blocks
print("🙂")

# https://www.unicode.org/cldr/charts/latest/annotations/
print("\N{fire}")
print("🙂")
print("Ünicöde")
print("𐐷")
print("لثالثَ عَشَرَ")

var = "test@test3"
print(var, end='@')
print()
print(var)


def my_function(x=20):
    """

    :type x: int
    """
    return x * 2


print(my_function(100))

def power(a, b):
    return a ** b


print(power(2, 10))
