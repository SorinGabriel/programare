r"""
1. Add type checks for other basic types
    a. complex
    b. byte
>>> print(type((1-1j)))
<class 'complex'>
>>> print(type(b'abcd'))
<class 'bytes'>

2. Check the type of 'Quick fox jumps over the lazy brown dog.'. What is it?
>>> print(type('Quick fox jumps over the lazy brown dog.'))
<class 'str'>

3. Check types of the following.
    a. 300
    b. 300.0
    c. "300.0"
>>> print(type(300))
<class 'int'>
>>> print(type(300.0))
<class 'float'>
>>> print(type("300.0"))
<class 'str'>

4. Are they the same?
>>> print(type(300) == type(300.0) == type("300.0"))
False

5. What is the type of 2+2?
>>> print(type(2+2))
<class 'int'>

"""
