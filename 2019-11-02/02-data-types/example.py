print(type("What is my type?"))
print(type(-17))
print(type(123.4))
print(type(False))
print(type(None))
print(type(b"UBBER#"))
print(type("UBBER#"))
print(b"UBBER#")
print(type(complex(2, 3)))
print(type(2j+4))
print(type(20+40j))
x = complex(4,5)
print(x.real)
print(x.imag)
print(type("Quick fox jumps over the lazy brown dog."))
print(type(300))
print(type(300.0))
print(type("300"))
print(type(300) == type("300.0"))
print(type(300) == type(300.0))
print( type(300.0) == type("300.0"))
print(type(2+2))
print(type("2"+"2"))

intNumber = 100
intNumber2 = 10
print(intNumber // intNumber2)
print(intNumber / intNumber2)

intNumber3 = 3
print(intNumber3 // intNumber2)
print(intNumber3 / intNumber2)
print(intNumber2 // intNumber3)
print(intNumber2 / intNumber3)

intNumber4 = 2
intNumber4 += intNumber4
print(intNumber4)

intNumber4 += 10
print(intNumber4)

intNumber4 += intNumber4
print(intNumber4)

var10 = None
var11 = None
print(var10 is var11)
print(var10 == var11)
print(None == None)

var12 = 10
var12 = var12 << 1
print(var12)

var12 = var12 << 1
print(var12)

var12 = var12 << 1
print(var12)

var12 = var12 >> 1
print(var12)

var12 = var12 >> 1
print(var12)



var = 4+\
10
print(var)

