import unittest


class TestBuiltins(unittest.TestCase):

    def test_inst(self):
        self.assertTrue(isinstance("a", int))

    def test_inst2(self):
        self.assertTrue("b" in "babilon")


if __name__ == "__main__":
    unittest.main()
