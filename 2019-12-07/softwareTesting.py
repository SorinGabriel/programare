def fibbonaci(n=0):
    values = [0, 1]
    length = 2
    if n < 0:
        return "nu"
    elif n == 0:
        return values[0]
    while length <= n:
        temp = values[1]
        values[1] = values[1] + values[0]
        values[0] = temp
        length += 1
    return values[1]


print(f"Fibonacci 0: {fibbonaci(0)}")
print(f"Fibonacci 1: {fibbonaci(1)}")
print(f"Fibonacci 2: {fibbonaci(2)}")
print(f"Fibonacci 3: {fibbonaci(3)}")
print(f"Fibonacci 4: {fibbonaci(4)}")
print(f"Fibonacci 5: {fibbonaci(5)}")
print(f"Fibonacci 6: {fibbonaci(6)}")
print("...")
print(f"Fibonacci 100: {fibbonaci(100)}")
print(f"Fibonacci 101: {fibbonaci(101)}")





