"""
1. Write a class Bakery, which:
• has a common variable called storage (int)
• has a function called baker() which (for 10 loops) notes down current state of the
storage, sleeps for 0.1s and adds 1 to the storage, then sleeps for 1s
• has a function called customer() which (for 10 loops) notes down current state of the
storage, sleeps for 0.2s and removes 2 from the storage if storage >= 2, then sleeps
for 1s
2. use threading.Lock to prevent race conditions
3. Run 2 customer threads and 1 baker thread
4. Use logging to ensure all functions are working
101
"""
import logging
import time
import concurrent.futures
import threading

l = logging.getLogger()
l.setLevel(logging.DEBUG)
l = logging.getLogger("toll_booth")
h = logging.StreamHandler()
f = logging.Formatter("%(asctime)s: %(message)s")
h.setFormatter(f)
l.addHandler(h)
l.setLevel(logging.INFO)

class Bakery:
    def __init__(self):
        self.storage = 0
        self.__lock = threading.Lock()

    def baker(self):
        for _ in range(10):
            l.info(f"Before baking: {self.storage}")
            with self.__lock:
                time.sleep(0.1)
                self.storage += 1
                time.sleep(1)
            l.info(f"After baking: {self.storage}")

    def customer(self, name):
        for _ in range(10):
            l.info(f"{name} before attempting to buy: {self.storage}")
            with self.__lock:
                if self.storage >= 2:
                    time.sleep(0.2)
                    self.storage -= 2
                    l.info(f"{name} bought: 2")
                time.sleep(1)


if __name__ == "__main__":
    start_time = time.time()
    bakeryInstance = Bakery()
    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        executor.submit(bakeryInstance.baker)
        executor.submit(bakeryInstance.customer, 'Gigel')
        executor.submit(bakeryInstance.customer, 'Ionel')


    delta_time = time.time() - start_time
    print(f"It took {delta_time:.1f}s.")
    l.info(f"Current state of the storage: {bakeryInstance.storage}")