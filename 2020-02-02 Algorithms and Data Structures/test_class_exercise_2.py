import pytest

"""
Daca dau un numar finit (e.g.: 5), parcurg de la 1 pana la acel numar si la stringul de returnat pun "a" pentru numar par
si "aa" pentru numar impar. Daca 
"""


def power_of(n, of):
    if n == 1:
        return True
    elif n < 1:
        return False
    else:
        return power_of(n / of, of)


def recursive_function(n):
    if n == 0:
        return ""
    elif n % 2 == 0:
        return "a" + recursive_function(n - 1)
    else:
        if power_of(n, 3):
            return "b" + recursive_function(n - 1)
        elif power_of(n, 5):
            return "c" + recursive_function(n - 1)
        return "aa" + recursive_function(n - 1)


@pytest.mark.parametrize(
    "number, of, expected_result",
    [
        (4, 2, True),
        (12, 2, False),
        (12, 3, False),
        (25, 5, True),
        (10, 10, True),

    ]
)
def test_power_of(number, of, expected_result):
    assert power_of(number, of) == expected_result


@pytest.mark.parametrize(
    "n,expected_result",
    [
        (0, ""),
        (1, "b"),
        (2, "ab"),
        (3, "bab"),
        (4, "abab"),
        (5, "cabab"),
    ]
)
def test_recursive_function(n, expected_result):
    assert recursive_function(n) == expected_result
