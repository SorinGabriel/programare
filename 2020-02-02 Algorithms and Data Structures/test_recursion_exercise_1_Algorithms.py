import pytest

"""
1. Write a function which returns a sum of integers in a given list recursively
Hint: sum(list) = list[0] + sum(list[1:])…
"""


def sum_recursive(input_list):
    """
    Recursively calculate the sum of all the elements within the list.
    :param input_list: List of integers. Raises TypeError if provided param is not a list or it contains a non-integer
    :return: The sum of all elements from the provided list or 0 for empty list
    """
    # Check that the input_list is a list, throw TypeError exception otherwise
    if not isinstance(input_list, list):
        raise TypeError
    # Return 0 if the input_list is empty
    if len(input_list) < 1:
        return 0
    # Return the value of the first index from the list size is equal to 1
    elif len(input_list) == 1:
        # Check that the value at the given index is
        if not isinstance(input_list[0], int):
            raise TypeError
        else:
            return input_list[0]
    return input_list[0] + sum(input_list[1:])


"""
2. * Write a function which calculates a given element of Fibonacci sequence.
Fibonacci sequence is defined as follows:
• F(0) = 0
• F(1) = 1
• F(x) = F(x-1) + F(x-2)
Hint: F(2) = F(1) + F(0) = 1 + 0 = 1, F(3) = F(2) + F(1) …
"""


def fibonacci_recursive(n):
    """
    Get the n-th number from the Fibonacci sequence. Note that the sequence starts with 0 index.
    :param n: Fibonacci sequence index. Raises TypeError if this param is not an integer or is negative.
    :return: Value from the input index
    """
    # Check that the provided index is integer and higher than 0, raise TypeError otherwise
    if not isinstance(n, int) or n < 0:
        raise TypeError
    if n < 2:
        return n
    return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2)


@pytest.mark.parametrize(
    "list_input,expected_result",
    [
        ([], 0),
        ([1], 1),
        ([1, 4], 5),
        ([-1, -2], -3),
        ([-5, 5, 10], 10),
        ([0, 0, 0], 0),
        ([0, 100, 4, 2, -3], 103),
    ]
)
def test_sum_recursive(list_input, expected_result):
    assert sum_recursive(list_input) is expected_result


@pytest.mark.parametrize(
    "n,expected_result",
    [
        (0, 0),
        (1, 1),
        (2, 1),
        (7, 13),
        (8, 21),
        (10, 55),
        (12, 144),
    ],
)
def test_fibonacci_recursive(n, expected_result):
    assert fibonacci_recursive(n) is expected_result


@pytest.mark.parametrize(
    "list_input,expected_result",
    [
        ((), 0),
        ({}, 0),
        ("", 0),
        (10, 0),
        ({"b": [1, 2]}, 0),
        ([{}, "", ()], 0),
        ([[]], 0),
        (-5, 0),
        ([[], "", 2], 0),
        (["", 5], 0),
        ({2, 5}, 7),
        ((2, 3), 5)
    ]
)
def test_sum_recursive_invalid_types(list_input, expected_result):
    with pytest.raises(TypeError):
        assert sum_recursive(list_input) is expected_result


@pytest.mark.parametrize(
    "n,expected_result",
    [
        (0.7, 0),
        ("", 0),
        ([], 0),
        ((), 0),
        ({}, 0),
        ("5", 0),
        (-1, 0)
    ]
)
def test_fibonacci_recursive_invalid_types(n, expected_result):
    with pytest.raises(TypeError):
        assert fibonacci_recursive(n) is expected_result


