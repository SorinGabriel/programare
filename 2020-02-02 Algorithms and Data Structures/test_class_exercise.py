import pytest

"""
Daca dau un numar finit (e.g.: 5), parcurg de la 1 pana la acel numar si la stringul de returnat pun "a" pentru numar par
si "aa" pentru numar impar
"""


def recursive_function(n):
    if n == 0:
        return ""
    if n % 2 == 0:
        return "a" + recursive_function(n - 1)
    else:
        return "aa" + recursive_function(n - 1)


@pytest.mark.parametrize(
    "n,expected_result",
    [
        (0, ""),
        (1, "aa"),
        (2, "aaa"),
        (3, "aaaaa"),
        (4, "aaaaaa"),
        (5, "aaaaaaaa")
    ]
)
def test_recursive_function(n, expected_result):
    assert recursive_function(n) == expected_result
