recipe = ("boil water", "insert egg", "wait 5min", "eat")
#1
print(f"Getting the 3rd step from the recipe: {recipe[2]}")

#2
print(f"Slice of the last two steps of the recipe {recipe[2:4]}")
print(f"Slice of the last two steps of the recipe {recipe[-2:]}")
#3
print(f"Occurences of 'wait 5min' using count() function: {recipe.count('wait 5min')}")

#4
print(f"Checking if 'boil water' is the first step by using index() function : {recipe.index('boil water') == 0}")