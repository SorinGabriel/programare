fruit = ["apple", "banana", "orange"]
price = [1.5, 3.0, 0.75]

print(zip(fruit, price))
print(list(zip(fruit, price)))
print(dict(zip(fruit, price)))
