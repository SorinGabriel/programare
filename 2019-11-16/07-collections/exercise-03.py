r"""
Given dictionary {1: "one", 2: "two", 3: "three"}
>>> d = {1: "one", 2: "two", 3: "three"}

1. Use built-in function `len()` to print its length
>>> len(d)
3

2. Using setitem operator `[]`, add a new key-pair: 4:"four"
>>> d[4] = "four"

3. Using getitem operator `[]` get the value assigned to key `2`
>>> d[2]
'two'

4. Using getitem operator `[]` try getting value for unassigned key, like `10`. What happens?
>>> try:
...     print(d[10])
... except KeyError:
...     pass

5. Using dictionary function `get()` get the value for key `10`. What happens?
>>> d.get(10) == None
True

6. Using dictionary function `get(key, default)` get the value for key `10`, this time setting default to `"unknown"`.
>>> d.get(10, "unknown")
'unknown'

7. Using dictionary function `get(key, default)` get the value for key `3`. Set default to `"unknown"`.
>>> d.get(3, "unknown")
'three'

8. Use dictionary function `items()`. What does it return? Is it a list?
>>> print(d.items())
dict_items([(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')])
>>> type(d.items())
<class 'dict_items'>

9. Use dictionary functions `keys()` and `values()`. Note similarities to `items()`.
>>> print(d.keys())
dict_keys([1, 2, 3, 4])
>>> print(d.values())
dict_values(['one', 'two', 'three', 'four'])

10. Use dictionary function `pop()` to get value assigned to `2`. Print the dictionary after using `pop()`.
>>> d.pop(2)
'two'
>>> d
{1: 'one', 3: 'three', 4: 'four'}

11. Use dictionary function `popitem()`. Print the dictionary after using `popitem`.
>>> d.popitem()
(4, 'four')
>>> d
{1: 'one', 3: 'three'}

12. Using `setdefault()` try to create key `2` with value `"two"`.
>>> 2 in d
False
>>> d.setdefault(2, "two")
'two'
>>> d[2]
'two'

13. Using `setdefault()` try to create key `3` with value `"new-three"`.
>>> 3 in d
True
>>> d.setdefault(3, "new-three")
'three'
>>> d[3]
'three'

14. Create a new dictionary `{0: "zero"}`. Using `update()` update main dictionary with values from the new dictionary.
>>> d.update({0: "zero"})
>>> d
{1: 'one', 3: 'three', 2: 'two', 0: 'zero'}

15. Using `clear()`, clear the dictionary.
>>> d.clear()
>>> d
{}

16. Using `fromkeys()` and a list of keys `["x", "y", "z"]`, initialize a dictionary with all values set to `False`.
>>> d = {}.fromkeys(["x", "y", "z"], False)
>>> d
{'x': False, 'y': False, 'z': False}

"""
