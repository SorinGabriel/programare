#number = input("Input a number: ")
#print(f"{number} mod 49: {int(number) % 49}")


#-------------------------------------------------
address_book = {
    "Helen": "838",
    "Theresa": "1224",
    "John": "2742",
    "William": "531"
}

name = input("Input a name: ")
#4, 5, 6
address = address_book.get(name)
if address:
    print(f"Address is: {address}")
else:
    print("Name is not in address book")
