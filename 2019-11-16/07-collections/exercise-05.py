r"""
Given following sets:
```
fish = {"cod", "salmon", "carp"}
birds = {"stork", "magpie"}
animals = {"cod", "stork"}
small_pond = {"carp"}
```
>>> fish = {"cod", "salmon", "carp"}
>>> birds = {"stork", "magpie"}
>>> animals = {"cod", "stork"}
>>> small_pond = {"carp"}

1. Using membership operator `in`, test whether `"salmon"` is a member of each of the sets.
>>> "salmon" in fish
True
>>> "salmon" in birds
False
>>> "salmon" in animals
False

2. Using `isdisjoint` set function, check whether fish and birds are disjoint. Sets are
disjoint when they have no common element.
>>> fish.isdisjoint(birds)
True

3. Perform the same check for fish and animal sets.
>>> fish.isdisjoint(animals)
False

4. Using `issubset`, check whether `small_pond` is a subset of `fish`. Set is called a
subset of X when all its elements are also members of set X.
>>> small_pond.issubset(fish)
True

5. Using `issuperset`, check whether `animals` is a superset of `birds`. Set X is called a
superset of Y when every element of set Y is also a member of set X.
>>> animals.issuperset(birds)
False

6. Using `union`, create a new set containing all members of `bird` and `fish`. Assign the
set to `animals`. Print the new `animals` set.
>>> animals = birds.union(fish)
>>> sorted(list(animals))   # set are not ordered; we need a deterministic test
['carp', 'cod', 'magpie', 'salmon', 'stork']

7. Using `intersection`, print a new set of all members found in both `small_pond` nd
`fish`.
>>> small_pond.intersection(fish)
{'carp'}

8. Using `difference`, print a new set of all members found in `fish`, but not in a
`small_pond`.
>>> diff = fish.difference(small_pond)
>>> sorted(list(diff))
['cod', 'salmon']

9. Using `update()`, update `animals` a new set: `pets = {"cat", "dog"}`.
>>> pets = {"cat", "dog"}
>>> animals.update(pets)
>>> "cat" in animals and "dog" in animals
True

10. Using `intersection_update` update `fish`, keeping only elements common with
`small_pond`.
>>> fish.intersection_update(small_pond)
>>> small_pond.issuperset(fish)
True

11. Using `difference_update` update `birds`, removing elements found in `pets`.
>>> old_birds = birds.copy()
>>> birds.difference_update(pets)
>>> old_birds == birds
True

12. Using `symmetric_difference_update` update set `a = {1, 2, 3}` using set b `b = {3, 4,
5}`. Symmetric difference will keep elements found in either set, but not in both.
>>> a = {1, 2, 3}
>>> b = {3, 4, 5}
>>> a.symmetric_difference_update(b)
>>> b
{3, 4, 5}
>>> a
{1, 2, 4, 5}

13. Using `remove`, remove `"cod"` from animals.
>>> animals.remove("cod")
>>> "cod" not in animals
True

14. Using `pop` remove and return an arbitrary element from animals.
>>> before = len(animals)
>>> removed_elem = animals.pop()
>>> removed_elem not in animals
True
>>> len(animals) == (before - 1)
True

15. Using `discard` try removing `"carp"` and `"shark"` from `fish` set. Does it fail when
you are trying to remove a non-existent element?
>>> fish
{'carp'}
>>> fish.discard("carp")
>>> fish
set()
>>> fish.discard("shark")
>>> fish
set()

"""
