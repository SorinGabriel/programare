calling_codes = {
    "Canada": 1,
    "USA": 1,
    "Morocco": 212,
    "Sudan": 249,
    "Iceland": 354,
    "China": 86,
}
print("Canada" in calling_codes)
print("Germany" in calling_codes)

prefixes = [1, 1, 212]
print(prefixes == calling_codes.values())
prefixes = [1, 1, 212, 249, 354, 86]
print(prefixes == calling_codes.values())
print(prefixes == list(calling_codes.values()))

copied = calling_codes.copy()
print(copied == calling_codes)
copied.popitem()
print(copied == calling_codes)
