dict = {
    1: "one",
    2: "two",
    3: "three"
}
#1
print(f"Length of the dictionary is {len(dict)}")

#2
dict[4] = "four"
print(f"Updated dictionary is {dict}")

#3
print(f"The value assigned to key 2 is {dict[2]}")

#4
#print(f"Trying to get the value of an unassigned key using the get-item operator []: {dict[10]} . Conclusion: throws KeyError: 10 error")

#5
print(f"Trying to get the value of an unassigned key using the dictionary function get(): {dict.get(10)}")

#6
print(f"Trying to get the value of an unnasigned key using the dictionary function get(key, default) and setting default to 'unknown': {dict.get(10, 'unknown')}")

#7
print(f"Trying to get the value of key 3 using the dictionary function get(key, default) and setting default to 'unknown': {dict.get(3, 'unknown')}")

#8
dictItems = dict.items()
print(f"Type of the return value using dictionary function items(), {dictItems}, is {type(dictItems)}")
print(f"Type of dictionary is: {type(dict)}")
#9
dictKeys = dict.keys()
print(f"Output using keys(): {dictKeys}. Type: {type(dictKeys)}")

dictValues = dict.values()
print(f"Output using keys(): {dictValues}. Type: {type(dictValues)}")

#10
print(f"Getting the value assigned to key 2 using the dictionary function pop(): {dict.pop(2)}")
print(f"Dictionary content is: {dict}")

#11
dict.popitem()
print(f"Dictionary after calling dictionary function popItem(): {dict}")

#12
dict.setdefault(2, "two")
print(f"Dictionary content after creating key 2 with value 'two' using setdefault() is: {dict}")

#13
dict.setdefault(3, "new-three")
print(f"Dictionary content after creating key 3 with value 'new-three' using setdefault() is: {dict}")
print(f"Does not work because key 3 already exists in the dictionary")
#14
dict.update({0: "zero"})
print(f"Dictionary content after creating a new dictionary and updating it with the newly created dictionary using dictionary function update(): {dict} ")
#15
dict.clear()
print(f"Dictionary content after calling dictionary function clear(): {dict}")
#16
newDict = dict.fromkeys(["x", "y", "z"], False)
print(dict)
print(f"Initialized a dictionary with x, y, z keys with False value assigned for all by using the dictionary function fromkeys() : {newDict}")
newDict = dict.fromkeys(["a", "b", "c"])
print(dict)
# Conclusion: you have to call fromkeys on a dictionary instance (whatever it is), but it won't change the state of the instance
print(f"Initialized a dictionary with a, b, c keys by using the dictionary function fromkeys() : {newDict}")
newDict = {}.fromkeys(["x", "y", "z"], False)
print(f"Initialized a dictionary with x, y, z keys with False value assigned for all by using the dictionary function fromkeys() : {newDict}")
newDict = {}.fromkeys(["a", "b", "c"])
print(f"Initialized a dictionary with a, b, c keys by using the dictionary function fromkeys() : {newDict}")
# Altceva -----------------------------------------------

def mea():
    ITEMS = 100
    mea = 100
    return "x"

print(mea())
print(mea)

