dictionary = {}  # this is an empty dictionary
print(f"Current length of 'dictionary': {len(dictionary)}")

# Let's add some key-value pairs.
dictionary["yes"] = "oui"
dictionary["dog"] = "chien"
dictionary["cat"] = "chat"
print(f"Dictionary: {dictionary} (length: {len(dictionary)})")

# Lookup
dog_fr = dictionary["dog"]
print(f"A 'dog' in French: {dog_fr}")

# Let's add some more key-value pairs
dictionary.update({"fish": "poisson", "mouse": "souris"})
print(f"Dictionary: {dictionary} (length: {len(dictionary)})")
