tournament_winners = ()  # this is an empty tuple
print(f"Current length of 'tournament_winners': {len(tournament_winners)}")

tournament_winners = ("Annie", "Basil", "Chet")
print(f"Winners: {tournament_winners} (length: {len(tournament_winners)})")

print(f"{tournament_winners[0]}: 1st place")
print(f"{tournament_winners[1]}: 2nd place")
print(f"{tournament_winners[2]}: 3rd place")

print(f"Places 2-3: {tournament_winners[1:]}")

# You cannot update a tuple. Next line will fail with TypeError.
tournament_winners[2] = "Chester"

