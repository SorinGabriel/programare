L1 = [1, 2, 5, 0]
L2 = L1[0:-2]
print("L1[0:-2] is: ")
print(L2)
L3 = L1[:-2]
print("L1[:-2] is: ")
print(L3)
L4 = L1[-2:]
print("L1[-2:] is: ")
print(L4)
L5 = L1[2:]
print("L1[2:] which is same as L1[2:4] is: ")
print(L5)
countItems = len(L1)

if countItems >= 2:
    L6 = L1[(countItems - 2):]
else:
    L6 = L1
print(L6)
# ------------------------------------
#     0   1   2   3   4   5
L = ["a", 1, "c", 7, "x", 84]
#    -6  -5  -4  -3  -2   -1
# 7, "x"
# slice with positive limits
L1 = L[3:5]
print(L1)
# slice with negative limits
L1 = L[-3:-1]
print(L1)
# displays empty array as the interval starts with higher number and ends with lower number (-2 > -3)
print(L[-2:-3])




