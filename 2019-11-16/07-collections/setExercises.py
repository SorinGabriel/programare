fish = {"cod", "salmon", "carp"}
birds = {"stork", "magpie"}
animals = {"cod", "stork"}
small_pond = {"carp"}

#1
print("Testing whether 'salmon' is a member of each set")
print(f"salmon is in fish: {'salmon' in fish}")
print(f"salmon is in birds: {'salmon' in birds}")
print(f"salmon is in animals: {'salmon' in animals}")
print(f"salmon is in small_pod: {'salmon' in small_pond}")

#2
print(f"Check if fish and birds sets are disjoint using isdisjoint() set function: {fish.isdisjoint(birds)}")

#3
print(f"Check if fish and animals sets are disjoint using isdisjoint() set function: {fish.isdisjoint(animals)}")

#4
print(f"Check if small_pond set is a subset of fish set using issubset() set function: {small_pond.issubset(fish)}")

#5
print(f"Check if animals is a superset of birds using issuperset() set function: {animals.issuperset(birds)}")

#6
birdAndFish = birds.union(fish)
print(f"Created a new set containing all members of birds and fish: {birdAndFish}")
animals2 = animals
#animals.update(birdAndFish)
#print(f"animals set after assigning birdAndFish set to it: {animals}")
animals2.union(birdAndFish)
print(f"animals set after using union() with birdAndFish as parameter: {animals2}")
# I add value to animals, but animals2 gets updated too as they are equal (references)
animals.add('asd')
print(animals2)

#7
intersectionSet = small_pond.intersection(fish)
print(f"Created a new set of all members found in both small_pond and fish using intersection() set function: {intersectionSet}")

#8
print(f"Print a new set of all members found in fish, but not in a small pond using difference(): {fish.difference(small_pond)}")

#9
pets = {'cat', 'dog'}
animals.update(pets)
print(f"Updated animals set with pets using update() set function {animals}")

#10
fish.intersection_update(small_pond)
print(f"Updated fish keeping only elements common with small_pond using intersection_update() set function: {fish}")

#11
print(f"animals: {pets}")
print(f"birds: {birds}")
birds.difference_update(pets)
print(f"Updated birds removing elements found in pets using difference_update() function {birds}")

#12
a = {1, 2, 3}
b = {3, 4, 5}
a.symmetric_difference_update(b)
print(f"Updated set a using set b with symmetric_difference_update() function: {a}")

#13
print(f"animals before removing anything from it: {animals}")
animals.remove("cod")
print(f"animals set after removing 'cods' from it: {animals}")

#14
print(f"element removed and returned using pop() set function on animals set: {animals.pop()}")
print(f"animals set after using pop() set function: {animals}")

#15
print(f"fish: {fish}")
fish.discard("carp")
print(f"fish set after removing 'carp' using discard() set function: {fish}")

fish.discard("shark")
print(f"fish set after removing not existing value 'shark' using discard() set function: {fish}")