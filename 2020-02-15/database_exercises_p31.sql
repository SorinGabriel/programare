/*
1. Select everything from table: employees
*/ 
SELECT * FROM employees;

/*
2. Select only firstName and lastName from table: employees
*/
SELECT firstName, lastName from employees;

/*
3. Select all employees with lastName Johnson
*/
SELECT * FROM employees where lastName = 'Johnson';

/*
4. Select all employees whose lastName starts with J
*/
SELECT * FROM employees where lastName like 'J%';

/*
5. Select all employees whose lastName contains so
*/
SELECT * FROM employees where lastName like '%so%';

/*
6. Select all employees born after 1980
*/
SELECT * FROM employees where dateOfBirth >= '1980-01-01'; 

/*
7. Select all employees born after 1980 and whose firstName is John
*/
SELECT * FROM employees where dateOfBirth >= '1980-01-01' and firstName = 'John'; 

/*
8. Select all employees born after 1980 or whose firstName is John
*/
SELECT * FROM employees where dateOfBirth >= '1980-01-01' OR firstName = 'John'; 

/*
9. Select all employees whose lastName is not Jameson
*/
SELECT * FROM employees where lastName <> 'Jameson';

/*
10. Select the maximum salary
*/
SELECT MAX(salary) from employees;

/*
11. Select the minium salary
*/
SELECT MIN(salary) from employees;

/*
12. Select the average salary
*/
SELECT AVG(salary) from employees;