/*
1. Identify the type of relationship between tables employees and departments
Many - to - one
*/

/*
2. Add additional functionality to the employees table by allowing a specific employee to have a
manager. How would you model this considering the fact that the manager should also be an
employee?
*/
ALTER TABLE `humanresources`.`employees` 
ADD COLUMN `managerId` INT NULL AFTER `departmentId`;


/*
3. Make Sophie the manager of John and James
*/
UPDATE employees 
SET 
    managerId = (SELECT 
            employeeId
        FROM
            (SELECT 
                employeeId
            FROM
                employees
            WHERE
                firstName = 'Sofie') AS x)
WHERE
    firstName IN ('John' , 'James');

/*
4. Add additional functionality:
a. Database will store project information as well: projectId, description
b. Design the database in such a way that an employee will be able to work on multiple
projects and also multiple employees can work on the same project
*/
CREATE TABLE `humanresources`.`projects` (
  `projectId` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`projectId`));

CREATE TABLE `humanresources`.`employees_projects` (
  `epId` INT NOT NULL AUTO_INCREMENT,
  `employeeId` INT NOT NULL,
  `projectId` INT NOT NULL,
  PRIMARY KEY (`epId`));

/*
5. Insert two projects to the database:
a. Python - Cinema Web App
b. Java - Fitness Web App
*/
INSERT INTO `humanresources`.`projects` (`description`) VALUES ('Python - Cinema Web App');
INSERT INTO `humanresources`.`projects` (`description`) VALUES ('Java - Fitness Web App');

/*
6. Assign John and James to the Python project and Julie and Sofie to the Java project
*/
insert into employees_projects (employeeId, projectId) values (
(select employeeId from employees where firstName = 'Julie'), (select projectId from projects where description like 'Java%')
);

insert into employees_projects (employeeId, projectId) values (
(select employeeId from employees where firstName = 'Sofie'), (select projectId from projects where description like 'Java%')
);

insert into employees_projects (employeeId, projectId) values (
(select employeeId from employees where firstName = 'John'), (select projectId from projects where description like 'Python%')
);

insert into employees_projects (employeeId, projectId) values (
(select employeeId from employees where firstName = 'James'), (select projectId from projects where description like 'Python%')
);
