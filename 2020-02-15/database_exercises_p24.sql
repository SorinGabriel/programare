/*
1. Insert into table: employees a new entry:
a. employeeId - 1
b. firstName - John
c. lastName - Johnson
d. dateOfBirth - 1975-01-01
e. phoneNumber - 0-800-800-314
f. email - john@johnson.com
g. salary - 1000
*/
INSERT INTO employees
(employeeId,  firstName, lastName, dateOfBirth, phoneNumber, email, salary)
VALUES
(1, 'John', 'Johnson', '1975-01-01', '0-800-800-314', 'john@johnson.com', 100);

SELECT * FROM employees;

/*
2. Update dateOfBirth of John Johnson to 1980-01-01
*/
UPDATE employees
SET dateOfBirth = '1980-01-01'
WHERE firstName = 'John' AND lastName = 'Johnson';

SELECT * FROM employees;

/*
3. Delete everything from table: employees
*/
TRUNCATE employees;

SELECT * FROM employees;

/*
4. Add two more entries in employees:
a. 1, 'John','Johnson', '1975-01-01', '0-800-800-888','john@johnson.com', 1000
b. 2, 'James','Jameson', '1985-02-02', '0-800-800-999','james@jameson.com', 2000
*/
INSERT INTO employees
(employeeId,  firstName, lastName, dateOfBirth, phoneNumber, email, salary)
VALUES
(1, 'John','Johnson', '1975-01-01', '0-800-800-888','john@johnson.com', 1000),
(2, 'James','Jameson', '1985-02-02', '0-800-800-999','james@jameson.com', 2000);

SELECT * FROM employees;

