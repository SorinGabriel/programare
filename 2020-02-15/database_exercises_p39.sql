/*
1. Alter table: employees
a. Make employeeId column PRIMARY KEY, NOT NULL, AUTO INCREMENT
*/
ALTER TABLE `humanresources`.`employees` 
CHANGE COLUMN `employeeId` `employeeId` INT NOT NULL AUTO_INCREMENT ,
ADD PRIMARY KEY (`employeeId`);

/*
2. Add two more entries in employees, this time not setting the employeeId manually. See what
happens:
a. 'Julie', 'Juliette', '1990-01-01', '0-800-900-111', 'julie@juliette.com', 5000
b. 'Sofie', 'Sophia', '1987-02-03', '0-800-900-222', 'sofie@sophia.com', 1700
*/
INSERT INTO employees
(firstName, lastName, dateOfBirth, phoneNumber, email, salary)
VALUES
('Julie', 'Juliette', '1990-01-01', '0-800-900-111', 'julie@juliette.com', 5000),
('Sofie', 'Sophia', '1987-02-03', '0-800-900-222', 'sofie@sophia.com', 1700);

/*
3. Create a new table: departments, with the following columns:
a. departmentId - Integer, PRIMARY KEY, NOT NULL, AUTO INCREMENT
b. name - Varchar, NOT NULL
*/
CREATE TABLE departments(
departmentId INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
name VARCHAR(25) NOT NULL
);

/*
4. Add two entries in table departments
a. HR
b. Finance
*/
INSERT INTO departments
(name)
VALUES
('HR'),
('Finance');

/*
5. Connect the two tables together - employees should have a reference to departments
a. Add a new column to the table employees: departmentId - Integer
b. Assign John to HR and Julie to Finance
*/
ALTER TABLE `humanresources`.`employees` 
ADD COLUMN `departmentId` INT(11) NULL AFTER `salary`;

UPDATE employees set departmentId = (SELECT departmentId FROM departments where name = 'HR') where firstName = 'John';
UPDATE employees set departmentId = (SELECT departmentId FROM departments where name = 'Finance') where firstName = 'Julie';

