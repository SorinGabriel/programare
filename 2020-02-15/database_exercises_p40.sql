USE humanresources;
/*
6. Delete entry HR from table: departments
a. Does this work?
b. Should we be able to delete it? If John is assigned to HR and we delete it is the data still
correct?
*/
DELETE from departments where name = 'HR';

/*
7. Create a foreign key from table employees to table departments
a. departmentId column from table employees should reference departmentId column from
table departments
b. remember naming convention: fk_employees_departments
*/
UPDATE `humanresources`.`employees` SET `departmentId` = 	NULL WHERE (`employeeId` = '1');

ALTER TABLE employees
    ADD FOREIGN KEY
    fk_employees_departments (departmentId)
    REFERENCES departments (departmentId);

/*
8. Now try to delete entry HR from table: departments
a. Does this still work?
*/
DELETE from departments where name = 'HR';

/*
9. Now try to add a new employee and set its departmentId as 10
a. Does this work? Should it?
b. Try to add this new employee and set its departmentId as 1. Does this work?
*/
INSERT INTO `humanresources`.`employees` (`firstName`, `lastName`, `dateOfBirth`, `phoneNumber`, `email`, `salary`, `departmentId`) VALUES ('Test', 'Test', '1987-02-03', '0-800-900-222', 'test@test.com', '1400', '10');
INSERT INTO `humanresources`.`employees` (`firstName`, `lastName`, `dateOfBirth`, `phoneNumber`, `email`, `salary`, `departmentId`) VALUES ('Test', 'Test', '1987-02-03', '0-800-900-222', 'test@test.com', '1400', '3');

/*
10. Try deleting the newly added employee
a. Does it work? Should it?
*/
DELETE FROM `humanresources`.`employees` WHERE (`employeeId` = '7');
