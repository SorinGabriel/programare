/*
1. Create a new database: humanResources
*/
CREATE DATABASE humanResources;
/*
Set 'humanResources' database as default schema
*/
USE humanResources;

/*
2. Create a new table: employees, with the following columns:
a. employeeId - Integer
b. firstName - Varchar
c. lastName - Varchar
d. dateOfBirth - Date
e. postalAddress - Varchar
*/
CREATE TABLE employees(
employeeId INT(11),
firstName VARCHAR(25),
 lastName VARCHAR(25),
 dateOfBirth DATE,
 postalAddress VARCHAR(25)
);

/*
3. Alter table: employees and add the following columns:
a. phoneNumber - Varchar
b. email - Varchar
c. salary - Integer
*/ 
ALTER TABLE employees
ADD phoneNumber VARCHAR(25) NOT NULL;

ALTER TABLE employees
ADD email VARCHAR(25) NOT NULL;

ALTER TABLE employees
ADD salary INT(4) NOT NULL;

/*
4. Alter table: employees and remove the following columns:
a. postalAddress
*/
ALTER TABLE employees
DROP COLUMN postalAddress;

/*
5. Create a new table: employeeAddresses
a. country - Varchar
*/
CREATE TABLE employeeAddresses(
 country VARCHAR(25)
);

/*
6. Remove table: employeeAddresses
*/
DROP TABLE employeeAddresses;

