/*
O noua baza de date 'AWS'. 
*/
CREATE SCHEMA `aws`;
/*
Set aws as default schema 
*/
USE aws;
/*
In aceasta baza de date vrem o tabela 'Angajati' cu coloanele:
ID_ang  INT
Nume_ang STR 
Prenume_ang STR
Salariu INT
Varsta INT
ID_Manager INT
ID_Branch INT
*/
CREATE TABLE Angajati (
  `ID_ang` int NOT NULL AUTO_INCREMENT,
  `Prenume_ang` varchar(45) DEFAULT NULL,
  `Nume_ang` varchar(45) DEFAULT NULL,
  `Salariu` int DEFAULT NULL,
  `Varsta` int DEFAULT NULL,
  `ID_Manager` int DEFAULT NULL,
  `ID_Branch` int DEFAULT NULL,
  PRIMARY KEY (`ID_ang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*
Tabela 'Branch' 
ID 
Nr_ang
Sediu
*/
CREATE TABLE `aws`.`branch` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Nr_ang` INT NULL,
  `Sediu` VARCHAR(45) NULL,
  PRIMARY KEY (`ID`));
ALTER TABLE `aws`.`branch` 
CHANGE COLUMN `Sediu` `Sediu` INT NULL DEFAULT NULL ;

/*
Tabela 'Sediu'
ID_Sediu
Nr_servere
Serviciu
*/
CREATE TABLE `aws`.`sediu` (
  `ID_Sediu` INT NOT NULL AUTO_INCREMENT,
  `Nr_servere` INT NULL,
  `Serviciu` INT NULL,
  PRIMARY KEY (`ID_Sediu`));

/*
Tabela 'Servicii'
ID_Serviciu 
Versiune VARCHAR
Cost INT
*/
CREATE TABLE `aws`.`servicii` (
  `ID_Serviciu` INT NOT NULL AUTO_INCREMENT,
  `Versiune` VARCHAR(45) NULL,
  `Cost` INT NULL,
  PRIMARY KEY (`ID_Serviciu`));

/*
Alter table la sediu sa punem 'nume' 
Alter table la branch sa punem 'nume'
Alter table la sercii sa punem 'nume'
*/
ALTER TABLE `aws`.`branch` 
ADD COLUMN `Nume` VARCHAR(45) NULL AFTER `ID`;
ALTER TABLE `aws`.`branch` 
CHANGE COLUMN `Nume` `Nume` VARCHAR(45) NOT NULL ;

ALTER TABLE `aws`.`sediu` 
ADD COLUMN `Nume` VARCHAR(45) NOT NULL AFTER `ID_Sediu`;

ALTER TABLE `aws`.`servicii` 
ADD COLUMN `Nume` VARCHAR(45) NOT NULL AFTER `ID_Serviciu`;

/*
Servicii
2 records:
'EC2', '2.1.1', '0.42 $/hr'
'S3', '0.1.1', ' 1.35 $/hr'
*/
ALTER TABLE `aws`.`servicii` 
CHANGE COLUMN `Cost` `Cost` FLOAT NULL DEFAULT NULL ;

INSERT INTO `aws`.`servicii` (`Nume`, `Versiune`, `Cost`) VALUES ('EC2', '2.1.1.', '0.42');
INSERT INTO `aws`.`servicii` (`Nume`, `Versiune`, `Cost`) VALUES ('S3', '0.1.1', '1.35');

/*
Sediu
3 records: 
'North Virginia', 1000, EC2
'North Ireland', 500, S3
'Frankfurt', 800, EC2
*/
INSERT INTO `aws`.`sediu` (`Nume`, `Nr_servere`, `Serviciu`) VALUES ('North Virginia', '1000', '1');
INSERT INTO `aws`.`sediu` (`Nume`, `Nr_servere`, `Serviciu`) VALUES ('North Ireland', '500', '2');
INSERT INTO `aws`.`sediu` (`Nume`, `Nr_servere`, `Serviciu`) VALUES ('Frankfurt', '800', '1');

/*
Branch
3 records: 
'NCSA', 400, 'North Virginia'
'EMEA', 1000, 'Frankfurt'
'APAC', 2000, 'Tokyo'
*/
INSERT INTO `aws`.`branch` (`Nume`, `Nr_ang`, `Sediu`) VALUES ('NCSA', '400', '1');
INSERT INTO `aws`.`branch` (`Nume`, `Nr_ang`, `Sediu`) VALUES ('EMEA', '1000', '3');

INSERT INTO `aws`.`sediu` (`Nume`, `Nr_servere`, `Serviciu`) VALUES ('Tokyo', NULL, NULL);
UPDATE `aws`.`sediu` SET `Nr_servere` = '1500', `Serviciu` = '2' WHERE (`ID_Sediu` = '4');

INSERT INTO `aws`.`branch` (`Nume`, `Nr_ang`, `Sediu`) VALUES ('APAC', '2000', '4');

/*
Angajati: 
Sorin Sorin 4000 28 6 NCSA

*/ 
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Sorin', 'Sorin', '4000', '28', '6', '1');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Florin', 'Florin', '1000', '67', '3', '2');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Sorin', 'Sorin', '10000', '26', '2', '2');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Ermina', 'Irina', '8000', '56', '1', '1');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Florentina', 'Ene', '14000', '22', '1', '1');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Andrei', 'Andrei', '2000', '72', '3', '2');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Andrei', 'Sfantul', '20000', '32', '0', '3');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Darie', 'Vladimir', '7000', '20', '6', '3');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Sergiu', 'Corleone', '100000', '47', '0', '3');
INSERT INTO `aws`.`angajati` (`Prenume_ang`, `Nume_ang`, `Salariu`, `Varsta`, `ID_Manager`, `ID_Branch`) VALUES ('Sorin', 'Tacut', '1250', '18', '2', '2');

/*
Cate servicii isi poate permite Corleone isi poate cumpara 
Florin - cate servicii imi permit
*/
SELECT 
	CONCAT(Prenume_ang, ' ', Nume_ang) as nume,
    servicii.Nume as serviciu,
    Salariu / Cost as ore
FROM
    angajati 
        INNER JOIN
    branch ON angajati.ID_Branch = ID
        INNER JOIN
    sediu ON branch.Sediu = ID_Sediu
        INNER JOIN
    servicii ON sediu.Serviciu = ID_Serviciu
WHERE
    (Nume_ang = 'Sorin' and Prenume_ang = 'Sorin');

