import csv

if __name__ == "__main__":
    people = []
    with open("data/people.csv") as f:
        reader = csv.DictReader(f)
        for row in reader:
            people.append(row)
    # Include the header too
    countRows = len(people) + 1
    # Count the people with middle name
    peopleWithMissingEmails = list(filter(lambda x: x['email'] == '', people))
    countPeopleWithoutEmailAddress = len(peopleWithMissingEmails)
    # Count the people with middle name
    countPeopleWithMiddleName = len(list(filter(lambda x: x['middle_name'] != '', people)))
    print(f"Number of all records: {countRows}")
    print(f"Number of people missing an email address: {countPeopleWithoutEmailAddress}")
    print(f"Number of people with middle name: {countPeopleWithMiddleName}")

    with open("data/people_new.csv", "w+") as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerow(row.keys())
        for row in peopleWithMissingEmails:
            writer.writerow(row.values())
