"""
Write a pattern matching the following e-mails. Test it.
    bjandourek0@stanford.edu
    mtimothy1@deviantart.com
    mdodding2@de-decms.com
    kwessing3@linkedin.com
    cstallion4@printfriendly.com
"""
import re

pattern = re.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])")
emails = [
    "bjandourek0@stanford.edu",
    "mtimothy1@deviantart.com",
    "mdodding2@de-decms.com",
    "kwessing3@linkedin.com",
    "cstallion4@printfriendly.com",
    "3c9@y.com",
    "c9@y.!",
    "!_____c.com"
]
for email in emails:
    match = pattern.search(email)
    if match:
        print(match.group())
