import re
if __name__ == "__main__":
    strings = [
        "A Scandal in Bohemia",
        "The Red-Headed League",
        "A Case of Identity",
        "The Boscombe Valley Mystery",
        "The Five Orange Pips"
    ]
    for expression in strings:
        match = re.search('^\w\w\w', expression)
        if match:
            print(match.group())
        else:
            print("Nope")