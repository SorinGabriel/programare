import csv


if __name__ == "__main__":
    with open("data/example.csv") as f:
        reader = csv.DictReader(f)
        for row in reader:
            print(dict(row))
