import json
import csv

if __name__ == "__main__":
    list_json = [
        {"id": 1, "lat": 16.3112941, "long": 104.8221147, "country": "Thailand", "city": "Don Tan"},
        {"id": 2, "lat": 41.400351, "long": -8.5116191, "country": "Portugal", "city": "Antas"}
    ]

    with open("data/file_json.json", "w+") as f:
        json.dump(list_json, f)

    with open("data/file_csv.csv", "w+", newline='') as f:
        writer = csv.DictWriter(f, fieldnames=list_json[0].keys())
        writer.writeheader()
        writer.writerows(list_json)
